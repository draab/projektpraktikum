# Projektpraktikum Implementation
###### tags: `Projektpraktikum`, `implementation`, `lineSensor`
----

TODO next: draw project design

~~Visual Paradigm (Community Edition) used for diagrams~~
~~EasyEDA: software for curcuits~~

## part list/shoppinglist
- [ ] Monokristallines Solar panel
- [x] Microcontroller (Arduino Uno/ESP8266/ESP32)
- [x] Line Sensor (TSL1401)
    - [ ] shading case
- [x] step motor
    - [x] motor driver
- [ ] meter circuit (INA219, bidirectional current/power monitor)
- [x] small display
- [ ] rack
    - [ ] some wood
    - [ ] printed gear circle
    - [ ] printed gear parts (one for step motor)

## Project Design

* component overview
  * small photovoltaics on rack with fixed slope
  * stepper motor for rotating the rack, planar (maybe with a gear) (fix it to 180° rotating ?)
  * microcontroller with motordriver (controlling angle of rack) (esp32 ? -> wifi)
  * linesensor TSL1401 (do i need the lense ?)

---

  * measure photovoltaic output voltage and current (which sensor)
      * cheap Maximum Power Point Tracking ?
      * only no-load voltage measurement or fix-load voltage measurement
      * DC DC converter ?
      * CN3791

  * small server (Raspberry Pi) [optional for tracking]
    * MQTT Broker (for recieving data, also possible to send data via subscription from the microcontroller)
    * InfluxDB (for storing data)
    * Grafana (for visualizing data)
    * Node-RED (for controling, via MQTT Broker)

---

  * more thoughts:
    * measure more metrics and interpret:
      * temperature, humidity
      * windiness
      * ... ?
    * adjustable slope (with second line sensor and second step motor)
    * how much energy needed for controlling and changing angle / engery gain

## Functional design

TODO what exacatlly to put here ?

 * more detailed design
   * construction of rack
   * gear
   * visualize "control algorithm"

## Code Architecture

 * classdiagram
 * handling sensor communication
 * Calibration
   * ongoing calibration
   * adujst integration time of sensor
   * set maximum integration time (very cloudy or night)
   * time based sleep ?
   * default angle ?
 * triggering motor via stepper driver
 * control algorithm (no PI(D) control system neccessary ?? -> three-point control system )

---

 * measure engery from photovoltaic
 * send metric to MQTT
 * subscribe MQTT topic for remote access (setting state, controlling)

## Dataflow

* how metrics travel from sensor to grafana



# others 

### topics for thesis

* bit over photovoltaik background
* mpp(t) - maximum power point (tracker)


### interesting links

[Stepper driver](https://elektro.turanis.de/html/prj143/index.html)

* [YT similar Project](https://www.youtube.com/watch?v=_6QIutZfsFs)
* [YT power meter/logger](https://www.youtube.com/watch?v=lrugreN2K4w)
* [YT photovoltaik](https://www.youtube.com/watch?v=WdP4nVQX-j0)

* [Übersicht 3D Modelseiten](https://all3dp.com/de/1/3d-modell-kostenlos-download-besten-webseiten/)

* Getriebe Zeichnen
    * [YT getriebe Zeichnen](https://www.youtube.com/watch?v=v0yadXyEMxE)
    * [YT Planetengetriebe](https://www.youtube.com/watch?v=xlho1DjwCbc)
    * [YT Schneckengetriebe](https://www.youtube.com/watch?v=xcBGcbTHMyU)