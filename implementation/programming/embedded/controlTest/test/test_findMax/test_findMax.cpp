
#define SENSOR_ARRAY_LENGTH 10
#define MAX_LEVEL_THRESHOLD_PERCENTAGE 0.25

#include <solarController.h>
#include <unity.h>

uint16_t test1[SENSOR_ARRAY_LENGTH] = {10,11,145,134,150,  20,22,33,11,33};
uint16_t test2[SENSOR_ARRAY_LENGTH] = {200,231,231,222,299,  320,322,333,311,233};


void controller_find_min_max_1(void) {
  SolarController control = SolarController(test1, SENSOR_ARRAY_LENGTH);

  //############# find min and max #############

  control.calculateMinMax();

  TEST_ASSERT_EQUAL(10, control.getMin());
  TEST_ASSERT_EQUAL(0, control.getMinIdx());
  TEST_ASSERT_EQUAL(150, control.getMax());
  TEST_ASSERT_EQUAL(4, control.getMaxIdx());
}

void controller_find_min_max_2(void) {
  SolarController control = SolarController(test2, SENSOR_ARRAY_LENGTH);
  
  //############# find min and max #############

  control.calculateMinMax();

  TEST_ASSERT_EQUAL(200, control.getMin());
  TEST_ASSERT_EQUAL(0, control.getMinIdx());
  TEST_ASSERT_EQUAL(333, control.getMax());
  TEST_ASSERT_EQUAL(7, control.getMaxIdx());
}

/*
void controller_find_min_max_3(void) {
  SolarController control = SolarController(test3, SENSOR_ARRAY_LENGTH);
  
  //############# find min and max #############

  control.calculateMinMax();

  TEST_ASSERT_EQUAL(92, control.getMin());
  TEST_ASSERT_EQUAL(32, control.getMinIdx());
  TEST_ASSERT_EQUAL(32, control.getMax());
  TEST_ASSERT_EQUAL(32, control.getMaxIdx());
}
*/


void controller_find_max_range_1(void) {
  SolarController control = SolarController(test1, SENSOR_ARRAY_LENGTH);
  
  //############# find min and max #############
  control.calculateMinMax();

  //############# find start and end of max #############
  control.calculateMaxRegion(MAX_LEVEL_THRESHOLD_PERCENTAGE);

  
  TEST_ASSERT_EQUAL(2, control.getMaxStartIdx());
  TEST_ASSERT_EQUAL(4, control.getMaxEndIdx());
  TEST_ASSERT_EQUAL(3, control.getMaxMiddleIdx());
}

void controller_find_max_range_2(void) {
  SolarController control = SolarController(test2, SENSOR_ARRAY_LENGTH);
  
  //############# find min and max #############
  control.calculateMinMax();

  //############# find start and end of max #############
  control.calculateMaxRegion(MAX_LEVEL_THRESHOLD_PERCENTAGE);

  
  TEST_ASSERT_EQUAL(5, control.getMaxStartIdx());
  TEST_ASSERT_EQUAL(8, control.getMaxEndIdx());
  TEST_ASSERT_EQUAL(6, control.getMaxMiddleIdx());
}

int main(int argc, char **argv) {


  UNITY_BEGIN();
  RUN_TEST(controller_find_min_max_1);
  RUN_TEST(controller_find_min_max_2);
  // RUN_TEST(controller_find_min_max_3);
  RUN_TEST(controller_find_max_range_1);
  RUN_TEST(controller_find_max_range_2);
  UNITY_END();

  return 0;
}