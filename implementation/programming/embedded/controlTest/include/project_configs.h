#ifndef __project_configs_h__
#define __project_configs_h__

#define SERIAL_BAUD_RATE 256000

#define SENSOR_ARRAY_LENGTH 128
#define MAX_LEVEL_THRESHOLD_PERCENTAGE 0.25



#endif // !__project_configs_h__