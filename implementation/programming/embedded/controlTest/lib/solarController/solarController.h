#ifndef solar_controller_h
#define solar_controller_h

#include <stdint.h>
#include <limits.h>

class SolarController {
    public:
        SolarController(uint16_t pixelArr[], const uint16_t pixelLength);

        uint16_t getMin();
        uint16_t getMax();
        uint16_t getMinIdx();
        uint16_t getMaxIdx();
        uint16_t getMaxStartIdx();
        uint16_t getMaxEndIdx();
        uint16_t getMaxMiddleIdx();
        void calculateMinMax();
        void calculateMaxRegion(float threshold);
    
    private:
        uint16_t *pixelArr;
        uint16_t arrLength, min, minIdx, max, maxIdx, maxStartIdx, maxEndIdx, maxMiddleIdx;
};


#endif //solar_controller_h