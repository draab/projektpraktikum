
#include <solarController.h>

SolarController::SolarController(uint16_t pixelArr[], const uint16_t arrLength) {
    this->pixelArr = pixelArr;
    this->arrLength = arrLength;
}

void SolarController::calculateMinMax() {
    
    uint16_t val;
    min = USHRT_MAX;
    max = 0;
    
    for(int i=0; i < arrLength; i++) {
        val = pixelArr[i];
        if(val < min)  {
            min = val;
            minIdx = i;
        }
        if(val > max) {
            max = val;
            maxIdx = i;
        }
    }
}

uint16_t SolarController::getMin() { return min; }
uint16_t SolarController::getMax() { return max; }

uint16_t SolarController::getMinIdx() { return minIdx; }
uint16_t SolarController::getMaxIdx() { return maxIdx; }

/**
 * threshold in percentage 0..1
 */
void SolarController::calculateMaxRegion(float threshold) {
    float range = (float) (max - min);

    float thresholdValue = max - (range * threshold);

    //take index of max and decrement the index while value is in threshold (threshold is a percentage value; 0.0 is min, 1.0 is max)
    for (maxStartIdx = maxIdx; maxStartIdx >= 0; maxStartIdx--)
    {
        if( *(pixelArr+maxStartIdx) < thresholdValue ) {
            maxStartIdx++;
            break;
        }
    }
    
    if(maxStartIdx < 0) maxStartIdx = 0;
    
    //do the same with incrementing the index
    for (maxEndIdx = maxIdx; maxEndIdx < arrLength ; maxEndIdx++)
    {
        if( *(pixelArr+maxEndIdx) < thresholdValue ) {
            maxEndIdx--;
            break;
        }
    }
    
    if(maxEndIdx >= arrLength) maxEndIdx = arrLength-1;

    // center of peak is the middle of the two founded indexs
    maxMiddleIdx = (maxStartIdx + maxEndIdx) / 2;
}


uint16_t SolarController::getMaxStartIdx() { return maxStartIdx; }
uint16_t SolarController::getMaxEndIdx() { return maxEndIdx; }
uint16_t SolarController::getMaxMiddleIdx() { return maxMiddleIdx; }