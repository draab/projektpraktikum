#include <Arduino.h>
#include "project_configs.h"

#include <solarController.h>

uint16_t test1[SENSOR_ARRAY_LENGTH] = {112,91,91,89,92,89,92,89,92,88,90,87,90,87,90,87,90,91,92,89,92,89,92,87,91,87,90,88,92,90,93,90,92,90,93,91,94,92,96,94,101,108,166,262,300,269,278,311,279,303,307,308,304,291,281,291,290,289,218,126,108,100,100,95,97,93,96,93,95,91,94,90,93,90,93,89,93,89,92,89,92,89,92,88,91,88,91,87,91,87,91,87,89,87,90,86,89,85,88,84,87,84,88,85,89,85,88,84,88,84,87,84,88,84,88,84,87,84,86,83,86,82,85,82,85,82,85,83};
uint16_t test2[SENSOR_ARRAY_LENGTH] = {101,77,78,75,78,74,79,74,78,74,76,73,77,73,77,74,76,76,78,75,79,75,78,73,77,73,77,74,78,74,79,75,78,75,78,75,79,75,79,75,80,79,83,84,102,99,106,112,112,113,119,114,123,114,117,115,119,114,120,111,105,96,96,90,92,87,89,85,87,82,85,80,83,78,81,77,80,76,80,76,79,76,79,75,79,75,78,75,79,74,78,74,77,74,77,74,77,73,76,72,76,73,77,73,77,73,76,72,76,72,76,72,76,72,76,72,75,71,74,71,74,70,74,70,74,70,74,70};
uint16_t test3[SENSOR_ARRAY_LENGTH] = {120,98,99,96,99,95,100,96,100,95,98,94,99,95,99,95,98,98,100,96,101,96,100,94,98,94,97,95,99,96,100,96,99,95,99,95,100,96,100,96,100,97,101,97,102,98,104,101,105,102,108,105,113,113,143,233,298,289,293,306,297,295,297,299,303,299,291,309,291,300,309,165,121,110,112,106,109,103,107,101,105,100,104,99,103,98,101,97,101,96,100,96,100,96,99,95,99,94,98,93,97,93,99,95,99,95,99,95,99,94,98,94,98,94,99,94,98,93,97,92,96,92,96,92,96,92,96,92};

uint16_t minIdx, maxIdx;

uint8_t sendData(uint16_t dataArr[], uint16_t length, uint16_t maxStartIdx, uint16_t maxEndIdx) {

    Serial.println("tsl1401");

    Serial.printf("maxStartIdx:%d\n", maxStartIdx);
    Serial.printf("maxEndIdx:%d\n", maxEndIdx);

    for(int i=0; i< length-1; i++) {

      Serial.print(dataArr[i]);
      Serial.print(";");
    }
    Serial.println(dataArr[length-1]);
    Serial.flush();
    
    return 0;
}

void setup() {
    Serial.begin(SERIAL_BAUD_RATE);
    

}

uint16_t iter = 0;
uint16_t * arr;

void loop() {
  switch(iter%3){
    case 0: arr = test1; break;
    case 1: arr = test2; break;
    case 2: arr = test3; break;
  }
  iter++;

  SolarController control = SolarController(arr, SENSOR_ARRAY_LENGTH);
  

  //############# find min and max #############

  control.calculateMinMax();

  //############# find start and end of max #############

  control.calculateMaxRegion(MAX_LEVEL_THRESHOLD_PERCENTAGE);

  sendData(arr, SENSOR_ARRAY_LENGTH, control.getMaxStartIdx(), control.getMaxEndIdx());


  delay(1000);
}

