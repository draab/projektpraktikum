#include <arduino.h>
/**
   TSL1401 adapter board pin definitions:
   Plug in your board with
*/
/* Configuration with adapter board hovering on top of the processor:
  int startPin=A0;
  int clkPin=A1;
  int gnd=A2;
  int aIn=A3;
  int vdd=A4;
*/
// configuration with adapter board outside Arduino board:
const int startPin = A4;
const int clkPin = A3;
const int gnd = A2;
const int aIn = A1;
const int vdd = A0;


// Sensor configuration..:
const int sensorLen = 128;

const int exposure = 30;

int ampls[sensorLen]; // the result array.

const int pxDelay = 2;

void setup() {
  // set up pin modes

  pinMode(gnd, OUTPUT);
  pinMode(vdd, OUTPUT);

  pinMode(startPin, OUTPUT);
  pinMode(clkPin, OUTPUT);
  
  pinMode(aIn, INPUT);


  // power up the chip:
  digitalWrite(startPin, 1);
  digitalWrite(clkPin, 0.);
  digitalWrite(gnd, 0);
  digitalWrite(vdd, 1);


  Serial.begin(9600);
}


// TSL1401 max. clock speed is 8MHz
// Arduinos are clocked at 16MHz -> do nothing much...
static inline void wait_us(long unsigned int d) {
  volatile int i = 0;
  for (int i = 0; i < d; i++) {
    i++;
    i--;
  }
}

// emits one clock pulse
static inline void clkPulse() {
  wait_us(pxDelay / 2);
  digitalWrite(clkPin, 1);
  wait_us(pxDelay);
  digitalWrite(clkPin, 0);
  wait_us(pxDelay / 2);
}

// read in a measurement
int measure()
{
  digitalWrite(startPin, 1);
  digitalWrite(clkPin, 1);
  wait_us(pxDelay);
  digitalWrite(startPin, 0);
  wait_us(pxDelay);
  digitalWrite(clkPin, 0);

  for (int i = 0; i < sensorLen; i++) {
    clkPulse();

    ampls[i] = analogRead(aIn);
    //fampls[i] = ao;
  }

  clkPulse(); // clock out the SI event

  return 0;
}

int print()
{
  // makes sure there are no trailing spaces
  for (int i = 0; i < sensorLen-1; i++) {
    Serial.print(ampls[i] );
    Serial.print(" ");
  }
  Serial.print(ampls[sensorLen-1] );
  Serial.println(); 
  return 0;
}

void loop() 
{
//  measure();
//  delay(exposure);

  measure();
  print();

}
