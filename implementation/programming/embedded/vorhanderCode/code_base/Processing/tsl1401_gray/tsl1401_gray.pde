import processing.serial.*;

Serial ser;

int stepX = 4;
int stepY = 256;

void setup()
{
  String list[] = Serial.list();

  printArray(list);

  // our STM32 board registers as a usb serial device;
  // for Linux, /dev/ttyACM* is a good candidate
  //
  for (int i=0;i<list.length;i++) {
    if (list[i].startsWith("/dev/tty.usbmodem")) {
      // Open the port you are using at the rate you want:
      ser = new Serial(this, list[i], 230400);
      if (ser == null) {
        println("FAILED TO INIT!!!!");
      } else {
        println("USING Serial " + list[i]);       
        break;
      }
    }
  }

  size(512,512);
  noStroke();
  background(0xffffff);
}

float[] peakToFloat(String line)
{
  String tok[] = line.split("\t");
  float vals[] = new float[tok.length];
  
  try {
    for (int i=0;i<2; i++) {
      vals[i] = Float.parseFloat(tok[i]);
    }
  } catch (Exception e) {}
  return vals;
}

float[] toFloat(String line)
{
  String tok[] = line.split(" ");
  float vals[] = new float[tok.length];
  
  try {
    for (int i=0;i<tok.length; i++) {
      vals[i] = Float.parseFloat(tok[i]);
    }
  } catch (Exception e) {}
  return vals;
}

int lf=10;

int line=0;
void draw()
{
  if (ser == null) { print("."); return; }
  if (ser.available() > 0 ) {
    String inBuffer = ser.readStringUntil(lf);   
    if (inBuffer != null) {
      
      if (inBuffer.startsWith("P=")) {
        println(inBuffer);
        
        float vals[] = peakToFloat(inBuffer.substring(2));
        float peak = vals[0];
        println("P " + peak);
        
        stroke(128,0,0);
        
        line( peak * stepX, height, peak*stepX, height-128 );
        
        return;
      } else {
        println(inBuffer);
        float vals[] = toFloat(inBuffer);
        if (vals.length < 120) return; 

        stroke(255);
        fill (255, width, height/2);
        rect( 0, 256, width, height);
        
        stroke(40);
        float max=0;
        for (int i=1;i<vals.length;i++) {
          line( (i-1)*stepX, height - vals[i-1]*stepY, (i)*stepX, height - vals[i]*stepY);
          if (vals[i]>max) max=vals[i];    
        }
        

        // horizontal intensity plot 
        for (int i=1;i<vals.length;i++) {
          stroke( (vals[i]/max )*256);          
          point( line, i);
        }

        line++;
        if (line==width) line=0;
      }
    }
  }  
}