import processing.serial.*;

Serial ser;

int stepX = 4;
int stepY = 256;

void setup()
{
  String list[] = Serial.list();
hint(ENABLE_STROKE_PURE);
  printArray(list);

  // our STM32 board registers as a usb serial device;
  // for Linux, /dev/ttyACM* is a good candidate,
  // on Mac, serial ports start with tty.usbmodem or cu.usbmodem, followed 
  // by a number that indicates the position in the USB device tree.
  //
  for (int i=0;i<list.length;i++) {
    if (list[i].startsWith("/dev/tty.usbmodem")) {
      // Open the port you are using at the rate you want:
      ser = new Serial(this, list[i], 230400);
      if (ser == null) {
        println("FAILED TO INIT!!!!");
      } else {
        println("USING Serial " + list[i]);       
        break;
      }
    }
  }

  size(512,512);
  noStroke();
  background(0xffffff);
}

// helper function converting a line of tab-separated numbers into a float-array 
float[] peakToFloat(String line)
{
  String tok[] = line.split("\t");
  float vals[] = new float[tok.length];
  
  try {
    for (int i=0;i<2; i++) {
      vals[i] = Float.parseFloat(tok[i]);
    }
  } catch (Exception e) {}
  return vals;
}

float[] toFloat(String line)
{
  String tok[] = line.split(" ");
  float vals[] = new float[tok.length];
  
  try {
    for (int i=0;i<tok.length; i++) {
      vals[i] = Float.parseFloat(tok[i]);
    }
  } catch (Exception e) {}
  return vals;
}

int lf=10;

int xPos=0; // index of the column to draw the intensity plot

void drawIntensities( float vals[] )
{
    float max=0, min=1.;  
    for (int i=2;i<vals.length-2;i++) {
      if (vals[i] > max) max=vals[i];    
      if (vals[i] < min) min=vals[i];    
    }

   // print (min) ; print(" "); print(max); println();
/*
    min=.1;
    max=.9;
    */
    //println(max);
    // horizontal intensity plot 
    for (int i=1;i<vals.length;i++) {
      stroke( (vals[i]-min)/(max-min)*256 , 256 );          
      //stroke( vals[i]*256 , 255   );          
      point( xPos, i*2);
      point( xPos, i*2+1);
    }

    xPos++;
    if (xPos==width) xPos=0;
}

/// drawLineGraph plots a pixel-by-pixel intensity line plot.
//
void drawLineGraph(float vals[])
{
    stroke(255);
    fill (255, width, height/2);
    rect( 0, 256, width, height);
    
    stroke(40);

    for (int i=1;i<vals.length;i++) {
      line( (i-1)*stepX, height - vals[i-1]*stepY, (i)*stepX, height - vals[i]*stepY);
      if (i>124) {
        print("\t");
        print(vals[i]);
      }
    }
      println();
}

void draw()
{
  if (ser == null) { println("."); return; }
  if (ser.available() > 0 ) {
    String inBuffer = ser.readStringUntil(lf);   
    if (inBuffer != null) {
      
      if (inBuffer.startsWith("P=")) {
        //println(inBuffer);
        // plot the peak value as detected by the device:      
        
        float vals[] = peakToFloat(inBuffer.substring(2));
        float peak = vals[0];
        //println("P " + peak);
        
        // draw a line where the peak is supposed to be. Expect a little offset due to rounding.        
        stroke(128,128,128);
        line( peak * stepX, height, peak*stepX, height-128 );
      } else if (inBuffer.startsWith("P=")) {
        println(inBuffer);
      } else {
        //println(inBuffer);
        float vals[] = toFloat(inBuffer);
        if (vals.length < 120) return; 

        drawLineGraph(vals);
  
        drawIntensities(vals);
      }
    }
  }  
}