import processing.serial.*;

Serial ser;

int stepX = 4;
float stepY = .25;

void setup()
{
  String list[] = Serial.list();
hint(ENABLE_STROKE_PURE);
  printArray(list);

  // our STM32 board registers as a usb serial device;
  // for Linux, /dev/ttyACM* is a good candidate,
  // on Mac, serial ports start with tty.usbmodem or cu.usbmodem, followed 
  // by a number that indicates the position in the USB device tree.
  //
  for (int i=0;i<list.length;i++) {
    if (list[i].startsWith("/dev/tty.usb")) {
      // Open the port you are using at the rate you want:
      ser = new Serial(this, list[i], 115200);
      if (ser == null) {
        println("FAILED TO INIT!!!!");
      } else {
        println("USING Serial " + list[i]);       
        break;
      }
    }
  }

  size(512,512);
  noStroke();
  background(0xffffff);
}


// convert the incoming json object
float[] toFloat(String line)
{
  int[] data = null;
  float vals[] = null;
  try {
    JSONObject json = parseJSONObject(line);
    data = json.getJSONArray("d").getIntArray();
    //println(data);
    vals = new float[data.length];
  } catch (Exception e) {
    println("Ouch! " + e.toString() );
    return null;
  }
  int i=0;
  try {
    for (;i<data.length; i++) {
      vals[i] = data[i];
    }
  } catch (Exception e) {
    println("Ouch! " + e.toString() + " " + i);
  }
  return vals;
}

int lf=10;

int xPos=0; // index of the column to draw the intensity plot

float aMax=-1., aMin=-1.; // sliding average of amplitude min/max values
float rate=.95; // adaptation rate - increase for slower changes

void adaptMinMax( float vals[] )
{
    float max=0, min=1000.;  
    for (int i=2;i<vals.length-2;i++) {
      if (vals[i] > max) max=vals[i];    
      if (vals[i] < min) min=vals[i];    
    }
    if (aMax < 0.) {
      aMax = max;
      aMin = min;
    } else {
      aMax = aMax * rate + max *(1.-rate);
      aMin = aMin * rate + min *(1.-rate);
    }
}

// horizontal intensity plot at the bottom of the image
void drawIntensities( float vals[] )
{
    float max=aMax, min=aMin;  

    for (int i=1;i<vals.length;i++) {
      stroke( (vals[i]-min)/(max-min)*256  );          
      //stroke( vals[i]*256 , 255   );          
      point( xPos, i*2);
      point( xPos, i*2+1);
    }

    xPos++;
    if (xPos==width) xPos=0;
}

/// drawLineGraph plots a pixel-by-pixel intensity line plot.
//
void drawLineGraph(float vals[])
{
    stroke(255);
    fill (255, width, height/2);
    rect( 0, 256, width, height);
    
    stroke(40);

    for (int i=1;i<vals.length;i++) {
      line( (i-1)*stepX, height - vals[i-1]*stepY, (i)*stepX, height - vals[i]*stepY);
    }
}
/**  laplacian-based computation of sharpness - along the opencv version of:
 * cv::Laplacian(src_gray, dst, CV_64F);
 * cv::Scalar mu, sigma;
 * cv::meanStdDev(dst, mu, sigma);
 * double focusMeasure = sigma.val[0] * sigma.val[0];
 *
 * @return variance scaled by number of pixels
 */
float laps[];

float computeFocus(float vals[])
{
    //float lap[]={-1,2,-1};
    if ( laps == null) 
      laps = new float[vals.length];  
    laps[0]=0;
    laps[vals.length-1]=0;
    
    float meanLap=0;
    // compute laplacian and mean
    for (int i=1;i<vals.length-1;i++) {
      laps[i] = vals[i-1]*-1 + vals[i]* 2 +vals[i+1]*-1;
      meanLap += laps[i];
    }
    meanLap /= vals.length;

    float var=0;
    for (int i=0;i<vals.length;i++) {
      var += (laps[i] - meanLap) * (laps[i] - meanLap);
    }    
    
    return var ;
}

int lIdx=0, rIdx=0;

float findPeak(float vals[])
{
    float max=0;
    int idx=0;
    for (int i=1;i<vals.length-1;i++) {
      if (vals[i]>=max) {
        max = vals[i];
        idx = i;
      }
    }
    float ws = max;
    float weighted = idx * max;

    float half = (max + aMin) / 2;
    
    // search FWMH shoulders:
    for (int i=idx-1;i>=0;i--) {
      weighted += i * vals[i];
      ws += vals[i];
      
      if ( vals[i] <= half 
          || vals[i] > vals[i+1] // end of slope
          ) {
        lIdx = i;
        break;
      }
    }

    for (int i=idx+1;i< vals.length;i++) {
      weighted += i * vals[i];
      ws += vals[i];

      if ( vals[i] <= half 
          || vals[i] > vals[i-1]) { // end of slope
        rIdx = i;
        break;
      }
    }
    float centro = weighted / ws ; 
    
    println("left " + lIdx + " right " + rIdx + " max " + max + " maxIdx " + idx
        + "  wei " + weighted 
        + "  ws " + ws 
        + "   centro  " + centro 
        );
        
    return centro;
    
}

void draw()
{
  if (ser == null) { println("."); return; }
  if (ser.available() > 0 ) {
    String inBuffer = ser.readStringUntil(lf);   
    if (inBuffer != null) {
      
      println(inBuffer);
      float vals[] = toFloat(inBuffer);
      if (vals == null || vals.length < 120) 
        return; // something went wrong with data transmission

      adaptMinMax( vals );
      
      drawIntensities(vals);


      drawLineGraph(vals);

      float peak = findPeak(vals);

      stroke(255,240,128);
      line( peak*stepX, height, peak*stepX, height - aMax*stepY-20);


      if (true) { // plot focus: print value and draw a red line
        float f = computeFocus(vals);
        stroke(255,255,255);
        fill(255,255,255);
        rect(0,270,800, 10);

        stroke(0,0,0);
        fill(0,0,0);
        text("f: " + (f*1000), 10,280);
        text("min: " + (int)aMin, 300,280);
        text("max: " + (int)aMax, 400,280);
        println(f);
        stroke(255,0,0);
        rect(0,280, f*10000, 1);
      }
    }
  }  
}