import processing.serial.*;

Serial ser;

int stepX = 4;
int stepY = 256;

void setup()
{
  String list[] = Serial.list();
hint(ENABLE_STROKE_PURE);
  printArray(list);

  // our STM32 board registers as a usb serial device;
  // for Linux, /dev/ttyACM* is a good candidate,
  // on Mac, serial ports start with tty.usbmodem or cu.usbmodem, followed 
  // by a number that indicates the position in the USB device tree.
  //
  for (int i=0;i<list.length;i++) {
    if (list[i].startsWith("/dev/tty.usb")) {
      // Open the port you are using at the rate you want:
      ser = new Serial(this, list[i], 115200);
      if (ser == null) {
        println("FAILED TO INIT!!!!");
      } else {
        println("USING Serial " + list[i]);       
        break;
      }
    }
  }

  size(512,512);
  noStroke();
  background(0xffffff);
}

// helper function converting a line of tab-separated numbers into a float-array 
float[] peakToFloat(String line)
{
  String tok[] = line.split("\t");
  float vals[] = new float[tok.length];
  
  try {
    for (int i=0;i<2; i++) {
      vals[i] = Float.parseFloat(tok[i]);
    }
  } catch (Exception e) {}
  return vals;
}

float[] toFloat(String line)
{
  JSONObject json = parseJSONObject(line);
  int[] data = json.getJSONArray("d").getIntArray();
  println(data);
  float vals[] = new float[data.length];
  int i=0;
  try {
    for (;i<data.length; i++) {
      vals[i] = data[i];
    }
  } catch (Exception e) {
    println("Ouch! " + e.toString() + " " + i);
  }
  return vals;
}

float[] toFloatOldFormat(String line)
{
  String tok[] = line.split(" ");
  float vals[] = new float[tok.length];
  int i=0;
  try {
    for (;i<tok.length; i++) {
      vals[i] = Float.parseFloat(tok[i]) / 1024.;
    }
  } catch (Exception e) {
    println("Ouch! " + e.toString() + " " + i);
  }
  return vals;
}


int lf=10;

int xPos=0; // index of the column to draw the intensity plot

float aMax=-1., aMin=-1.; // sliding average of amplitude min/max values
float rate=.95; // adaptation rate - increase for slower changes

void adaptMinMax( float vals[] )
{
    float max=0, min=1.;  
    for (int i=2;i<vals.length-2;i++) {
      if (vals[i] > max) max=vals[i];    
      if (vals[i] < min) min=vals[i];    
    }
    if (aMax < 0.) {
      aMax = max;
      aMin = min;
    } else {
      aMax = aMax * rate + max *(1-rate);
      aMin = aMin * rate + min *(1-rate);
    }
}

// horizontal intensity plot at the bottom of the image
void drawIntensities( float vals[] )
{
    float max=aMax, min=aMin;  

    for (int i=1;i<vals.length;i++) {
      stroke( (vals[i]-min)/(max-min)*256  );          
      //stroke( vals[i]*256 , 255   );          
      point( xPos, i*2);
      point( xPos, i*2+1);
    }

    xPos++;
    if (xPos==width) xPos=0;
}

/// drawLineGraph plots a pixel-by-pixel intensity line plot.
//
void drawLineGraph(float vals[])
{
    stroke(255);
    fill (255, width, height/2);
    rect( 0, 256, width, height);
    
    stroke(40);

    for (int i=1;i<vals.length;i++) {
      line( (i-1)*stepX, height - vals[i-1]*stepY, (i)*stepX, height - vals[i]*stepY);
    }
}
/**  laplacian-based computation of sharpness - along the opencv version of:
 * cv::Laplacian(src_gray, dst, CV_64F);
 * cv::Scalar mu, sigma;
 * cv::meanStdDev(dst, mu, sigma);
 * double focusMeasure = sigma.val[0] * sigma.val[0];
 *
 * @return variance scaled by number of pixels
 */
float laps[];

float computeFocus(float vals[])
{
    //float lap[]={-1,2,-1};
    if ( laps == null) 
      laps = new float[vals.length];  
    laps[0]=0;
    laps[vals.length-1]=0;
    
    float meanLap=0;
    // compute laplacian and mean
    for (int i=1;i<vals.length-1;i++) {
      laps[i] = vals[i-1]*-1 + vals[i]* 2 +vals[i+1]*-1;
      meanLap += laps[i];
    }
    meanLap /= vals.length;

    float var=0;
    for (int i=0;i<vals.length;i++) {
      var += (laps[i] - meanLap) * (laps[i] - meanLap);
    }    
    
    return var ;
}

void draw()
{
  if (ser == null) { println("."); return; }
  if (ser.available() > 0 ) {
    String inBuffer = ser.readStringUntil(lf);   
    if (inBuffer != null) {
      
      if (inBuffer.startsWith("P=")) {
        //println(inBuffer);
        // plot the peak value as detected by the device:      
        
        float vals[] = peakToFloat(inBuffer.substring(2));
        float peak = vals[0];
        //println("P " + peak);
        
        // draw a line where the peak is supposed to be. Expect a little offset due to rounding.        
        stroke(128,128,128);
        line( peak * stepX, height, peak*stepX, height-128 );
        
    } else if (inBuffer.startsWith("E=")) {
        println(inBuffer);
      } else {
        println(inBuffer);
        float vals[] = toFloat(inBuffer);
        if (vals.length < 120) return; 

        adaptMinMax( vals );
        drawLineGraph(vals);
  
        drawIntensities(vals);

        { // plot focus: print value and draw a red line
          float f = computeFocus(vals);
          stroke(255,255,255);
          fill(255,255,255);
          rect(0,270,800, 10);
  
          stroke(0,0,0);
          fill(0,0,0);
          text("f: " + (f*1000), 10,280);
          println(f);        
          stroke(255,0,0);
          rect(0,280, f*10000, 1);
 
        }
      }
    }
  }  
}