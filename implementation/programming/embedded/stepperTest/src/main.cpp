#include <Arduino.h>
#include <CheapStepper.h>

/* #################  connecting driver board  #################
https://elektro.turanis.de/html/prj143/index.html

ULN2003 - Arduino
IN1	- 8
IN2	- 9
IN3	- 10
IN4	- 11
IN5	-
IN6	-
IN7	-
Vcc (+5..12V) -	5V
GND (-5..12V)	- GND
*/

int pin1 = 13;
int pin2 = 12;
int pin3 = 14;
int pin4 = 27;

CheapStepper stepper = CheapStepper(pin1, pin2, pin3, pin4);

bool moveClockwise = true;

void setup() {
  // put your setup code here, to run once:

}

void loop() {

  
    // move a full rotation (4096 mini-steps)
    for (int s=0; s<4096; s++){
        stepper.step(moveClockwise);
    }

    delay(1000);
    moveClockwise = !moveClockwise; 

}