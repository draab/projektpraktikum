#include <Arduino.h>

#include "project_config.h"
#include <OLEDController.h>

OLEDController c = OLEDController(I2C_PIN_OLED_SDA, I2C_PIN_OLED_SCL);

void setup() {

  c.init();
}


int16_t i = 0;

void loop() {

  c.displayAll(i++, SENSOR_ARRAY_LENGTH, SENSOR_TARGET_RANGE);

  delay(100);
}
