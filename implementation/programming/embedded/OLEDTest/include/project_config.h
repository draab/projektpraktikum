#ifndef __project_configs_h__
#define __project_configs_h__

#include "pinout.h"

#define SERIAL_BAUD_RATE 256000

#define SENSOR_ARRAY_LENGTH 128
#define MAX_LEVEL_THRESHOLD_PERCENTAGE 0.25


#define SENSOR_TARGET_RANGE 5

#endif // !__project_configs_h__