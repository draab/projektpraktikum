
#ifndef __OLEDController_h__
#define __OLEDController_h__

#include <Arduino.h>

// Include the correct display library
// For a connection via I2C using Wire include
#include <Wire.h>  // Only needed for Arduino 1.6.5 and earlier
#include <SSD1306Wire.h> // legacy include: `#include "SSD1306.h"`
// or #include "SH1106Wire.h", legacy include: `#include "SH1106.h"`


#define OLED_DISPLAY_WIDTH 128
#define OLED_DISPLAY_HEIGHT 64

#define OLED_SENSOR_BAR_HEIGHT 15




class OLEDController {
    private:
        // Initialize the OLED display using Wire library
        SSD1306Wire oledDisplay;
        
    public:
        OLEDController(int16_t i2cSDA, int16_t i2cSCL);

        void init();

        void drawSensorValue(uint16_t value, uint16_t sensorArrayLength, uint16_t targetRange);

        void drawSensorInfo(int16_t diff);

        void branding();

        void displayAll(int16_t value, uint16_t sensorArrayLength, uint16_t targetRange);
};


#endif