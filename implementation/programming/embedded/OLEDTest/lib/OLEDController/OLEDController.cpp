#include <OLEDController.h>

OLEDController::OLEDController(int16_t i2cSDA, int16_t i2cSCL) : oledDisplay(0x3c, i2cSDA, i2cSCL) {}

void OLEDController::init() {
            
    // Initialising the UI will init the display too.
    oledDisplay.init();

    oledDisplay.flipScreenVertically();
    oledDisplay.setFont(ArialMT_Plain_10);
}

void OLEDController::drawSensorValue(uint16_t value, uint16_t sensorArrayLength, uint16_t targetRange) {
    float ratio = ((float)OLED_DISPLAY_WIDTH) / sensorArrayLength;
    int16_t middle = OLED_DISPLAY_WIDTH / 2;

    int16_t val = value * ratio;

    //border
    oledDisplay.setColor(WHITE);
    oledDisplay.drawRect(0, 0, OLED_DISPLAY_WIDTH, OLED_SENSOR_BAR_HEIGHT);

    //target region
    oledDisplay.fillRect(middle - targetRange/2, 1, targetRange, OLED_SENSOR_BAR_HEIGHT-2);

    //value position
    oledDisplay.setColor(WHITE);
    oledDisplay.fillRect(val-1, 1, 3, OLED_SENSOR_BAR_HEIGHT - 2);
    oledDisplay.setColor(BLACK);
    oledDisplay.drawLine(val, 1, val, OLED_SENSOR_BAR_HEIGHT-2);

}

void OLEDController::drawSensorInfo(int16_t diff) {
        // Text alignment demo
    oledDisplay.setFont(ArialMT_Plain_16);
    oledDisplay.setColor(WHITE);

    // The coordinates define the left starting point of the text
    oledDisplay.setTextAlignment(TEXT_ALIGN_LEFT);
    char buffer[10];
    sprintf(buffer, "Diff: %d", diff);
    oledDisplay.drawString(0, OLED_SENSOR_BAR_HEIGHT, buffer);
}

void OLEDController::branding() {
    oledDisplay.setFont(ArialMT_Plain_10);
    oledDisplay.setColor(WHITE);
    
    oledDisplay.setTextAlignment(TEXT_ALIGN_RIGHT);
    oledDisplay.drawString(OLED_DISPLAY_WIDTH, OLED_DISPLAY_HEIGHT-10, "by Daniel Raab");
}

void OLEDController::displayAll(int16_t value, uint16_t sensorArrayLength, uint16_t targetRange) {
    // clear the display
    oledDisplay.clear();

    drawSensorValue(value % OLED_DISPLAY_WIDTH, targetRange, sensorArrayLength);
    drawSensorInfo((value++ % OLED_DISPLAY_WIDTH) - (OLED_DISPLAY_WIDTH/2));
    branding();

    // write the buffer to the display
    oledDisplay.display();
}