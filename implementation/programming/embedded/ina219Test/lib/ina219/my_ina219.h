#ifndef __my_ina219_h__
#define __my_ina219_h__

//#include <Wire.h>
#include <Adafruit_INA219.h>

class MyINA219 {
    private:
        Adafruit_INA219 ina219;
        float shuntvoltage_mV = 0;
        float busvoltage_V = 0;

    public:
        float current_mA = 0;
        float loadvoltage_V = 0;
        float power_mW = 0;

        void init() {
            if (! ina219.begin())
                Serial.println("Failed to find INA219 chip");
        };

        void readFromSensor() {
            shuntvoltage_mV = ina219.getShuntVoltage_mV();
            busvoltage_V = ina219.getBusVoltage_V();

            current_mA = ina219.getCurrent_mA();
            power_mW = ina219.getPower_mW();
            loadvoltage_V = busvoltage_V + (shuntvoltage_mV / 1000);
        }
};



#endif // !__my_ina219_h__