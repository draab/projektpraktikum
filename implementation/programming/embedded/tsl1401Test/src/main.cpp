#include <Arduino.h>
#include <serialComm.h>
#include <tsl1401esp32.h>

#define SERIAL_BAUD_RATE 256000


#define SI_PIN GPIO_NUM_33 //serial input pin - SI - starts the exposure
#define CLK_PIN GPIO_NUM_32 //clock pin - CLK // freq 5 - 2000 kHz
#define AO_PIN GPIO_NUM_34 //analog output from TSL1401 - AO

int64_t lastDataSend=0;
TSL1401_ESP32 tsl1401;

void setup(void) {
  initCommunication();
  
  tsl1401.initPins(CLK_PIN, SI_PIN, AO_PIN);
}

void loop(void) {

  tsl1401.readAverageWithOffset(10, 100);

  Serial.println("tsl1401");

  for(int i=0; i < SENSOR_ARRAY_LENGTH-1; i++) {
    Serial.printf("%.0f;", tsl1401.averageArr[i]);
  }
    Serial.printf("%.0f", tsl1401.averageArr[SENSOR_ARRAY_LENGTH-1]);
  Serial.println();

  delay(1000);
}