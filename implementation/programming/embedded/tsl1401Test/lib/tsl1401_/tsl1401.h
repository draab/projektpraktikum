#ifndef tsl1401_h
#define tsl1401_h

#include <Arduino.h>

#define SENSOR_ARRAY_LENGTH 128

#define SI_PIN A4 //serial input pin - SI - starts the exposure
#define CLK_PIN A3 //clock pin - CLK // freq 5 - 2000 kHz
#define GND_PIN A2 //ground - GND -> for direct connection to arduino dev board
#define AO_PIN A1 //analog output from TSL1401 - AO
#define VDD_PIN A0 //supply voltage - VDD -> for direct connection to arduino dev board

// todo change NOP to inline function for calculating time, NOP cycles for example

#define NOP __asm__ __volatile__ ("nop\n\t")    // 16Mhz -> 62,5 ns

class SensorData {
    public:
        static uint16_t pixelArr[SENSOR_ARRAY_LENGTH];
};

uint8_t initPins();

uint8_t readSensorLine();


#endif