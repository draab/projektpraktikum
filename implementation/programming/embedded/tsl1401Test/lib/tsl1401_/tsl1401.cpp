
/**  
 * sensor tsl1401 (https://ams.com/linear-array)
 * datasheet: https://ams.com/documents/20143/36005/TSL1401CL_DS000136_3-00.pdf/8de4cae4-354c-c2c3-8db4-6a132f969a0a
 * 
 * clk max 8 MHz
 *  
 * info:
*      -> each digitalWrite costs about 3,8 us
*      -> each analogRead costs about 100 us
*/


#include "tsl1401.h"

uint16_t SensorData::pixelArr[SENSOR_ARRAY_LENGTH];

static uint8_t powerUpSensor();
static uint8_t integrateLight(uint16_t clks);
static uint8_t outputClkSignals(uint16_t count);
static uint8_t readAnalogSensorArray();


uint8_t initPins () {

    //set pin modes
    pinMode(GND_PIN, OUTPUT);
    pinMode(VDD_PIN, OUTPUT);

    pinMode(CLK_PIN, OUTPUT);
    pinMode(SI_PIN, OUTPUT);

    pinMode(AO_PIN, INPUT);

//TODO check if this works:
 //   analogReadResolution(8);


    // enable pins for power -> for direct connection to arduino dev board
    powerUpSensor();

    return 0;
}

static uint8_t powerUpSensor() {

    digitalWrite(GND_PIN, LOW);
    digitalWrite(VDD_PIN, HIGH);

    digitalWrite(CLK_PIN, LOW);
    digitalWrite(SI_PIN, LOW);

    return 0;
}

/**
 * result is stored in pixelArr;
 */
uint8_t readSensorLine() {

    integrateLight(1000);
    //delay(500);

    readAnalogSensorArray();

    return 0;
}

/**
 * pre Clock phase for integrating 0,0645 - 100 ms
 * 
 * 4x digital write: 4x 3,8 us = 15,2 us + clks * 8us
 */
uint8_t integrateLight(uint16_t clks) {

    //clear integrater

    //start clearing with simulating fast read cicyle
    //start reading
    digitalWrite(SI_PIN, HIGH);
    NOP; NOP;
    digitalWrite(CLK_PIN, HIGH);
    NOP; NOP;
    digitalWrite(SI_PIN, LOW);
    NOP; NOP;
    digitalWrite(CLK_PIN, LOW);
    NOP; NOP;

    outputClkSignals(SENSOR_ARRAY_LENGTH);  //last clock is SENSOR_ARRAY_LENGTH + 1 for clearing AO output
    // integrating starts here already at clock 19
    // TODO ??? clock first 18 times slowly to fully reset the pixel integrators ????

    //extra clock cycles for more light integrating
    outputClkSignals(clks);

    return 0;
}


/**
 * outputs on CLK_PIN count high and lows; high time and low time are 2 NOP each;
 * 
 * digital write 2x 3,8 us =  7,6 us
 *         NOP   4x 62,5 ns = 250 ns
 *                      sum of ~ 8 us
 */
static uint8_t outputClkSignals(uint16_t count) {
    
    for(uint16_t i=0; i<count; i++) {
        NOP; NOP;
        digitalWrite(CLK_PIN, HIGH);
        NOP; NOP;
        digitalWrite(CLK_PIN, LOW);
    }

    return 0;
}


/**
 * info: each analogRead costs about 100 us
 * //TODO change to 8 bit read, to be faster...
 */
static uint8_t readAnalogSensorArray() {

    //integrating should already had be done
    //clock should be low here

    //sends SI pin HIGH to start reading pixels
    digitalWrite(SI_PIN, HIGH);

    //wait tsuSI (min 20 ns)
    NOP;

    // SI to low is done in readAnalogSensorArray function
    
    //starts with set clock high
    digitalWrite(CLK_PIN, HIGH);    // -> after 350ns AO is at +- 1%
    NOP; NOP; NOP; NOP; NOP;

    //wait thSI  (min 0 ns)
    
    digitalWrite(SI_PIN, LOW);

    //no time defined to wait
    //read first pixel value

    SensorData::pixelArr[0] = analogRead(AO_PIN);     //10 bit int value (0-1023)

    digitalWrite(CLK_PIN, LOW);

    for(int i = 1; i < SENSOR_ARRAY_LENGTH; i++) {
        NOP; //clock puls duration low: min 50ns

        digitalWrite(CLK_PIN, HIGH);     // -> after 350ns AO is at +- 1%
        NOP; NOP; NOP; NOP; NOP;
        digitalWrite(CLK_PIN, LOW);
        SensorData::pixelArr[i] = analogRead(AO_PIN);
    }

    digitalWrite(CLK_PIN, HIGH);    // SENSOR_ARRAY_LENGTH +1  for reset AO output //needs time to reset via pull down resistor
    NOP; NOP;

    digitalWrite(CLK_PIN, LOW);
    NOP; NOP;

    return 0;
}