/**  
 * sensor tsl1401 (https://ams.com/linear-array)
 * datasheet: https://ams.com/documents/20143/36005/TSL1401CL_DS000136_3-00.pdf/8de4cae4-354c-c2c3-8db4-6a132f969a0a
 * 
 *    clk max 8 MHz
 * 
 * esp32:
 *    clk 240MHz -> 4.17ns per cycle
 *      -> digitalWrite: 30 cycles -> 120ns
 *      -> analogRead: 2400 cycles -> 10 us 
 * 
 * 
 * https://github.com/QuaireMerlinLouis/TSL1401_ESP32/blob/master/TSL1401_ESP32_dualcore.ino
 * 
 * -> https://github.com/empierre/arduino/blob/master/TSL1401
 */


#include "tsl1401esp32.h"

/**
 * Init all pins for the tsl1401;
 * set ADC resolution to 10 bit
 */
void TSL1401_ESP32::initPins(uint8_t clkPin, uint8_t siPin, uint8_t aoPin) {
    this->clkPin = clkPin;
    this->siPin = siPin;
    this->aoPin = aoPin;

    analogReadResolution(10);
    
    //set pin modes
    pinMode(clkPin, OUTPUT);
    pinMode(siPin, OUTPUT);

    pinMode(aoPin, INPUT);

    digitalWrite(clkPin, LOW);
    digitalWrite(siPin, LOW);
    lastFClkEdge = esp_timer_get_time();
    lastSiEdge = lastFClkEdge;
    curPixelIdx = 0;
}


void TSL1401_ESP32::waitForTimerValue(int64_t timerValue) {
    do {
        curTimerValue = esp_timer_get_time();
    } while(curTimerValue < timerValue);
}


void TSL1401_ESP32::readSensor() {
    
    waitForTimerValue(lastReadClockCycle + PIXEL_CHARGE_TRANSFER_TIME_US);
    waitForTimerValue(integratingStart + INTEGRATION_MIN_TIME_US);

    digitalWrite(siPin, HIGH);
    lastSiEdge = esp_timer_get_time();

    waitForTimerValue(lastSiEdge + SI_MIN_SETUP_TIME_US);
    waitForTimerValue(lastFClkEdge + CLK_MIN_TIME_US);
    digitalWrite(clkPin, HIGH);
    lastRClkEdge = esp_timer_get_time();
    curPixelIdx = 0;

    // no time to wait t_h(SI) is 0ns; SI must simply be low before next rising clk
    digitalWrite(siPin, LOW);

    waitForTimerValue(lastRClkEdge + CLK_MIN_TIME_US);
    digitalWrite(clkPin, LOW);
    lastFClkEdge = esp_timer_get_time();
    

    while(curPixelIdx < SENSOR_ARRAY_LENGTH) {

        waitForTimerValue(lastRClkEdge + AO_SETTLING_TIME_US);
        internalPixelArr[curPixelIdx] = analogRead(aoPin);         // analog value of pixel index is read

        waitForTimerValue(lastFClkEdge + CLK_MIN_TIME_US);
        digitalWrite(clkPin, HIGH);
        lastRClkEdge = esp_timer_get_time();
        curPixelIdx++;

        if(curPixelIdx == 18) {  // for measuring t_int
            integratingStart = lastRClkEdge;
        }

        waitForTimerValue(lastRClkEdge + CLK_MIN_TIME_US);
        digitalWrite(clkPin, LOW);
        lastFClkEdge = esp_timer_get_time();
    }

    lastReadClockCycle = esp_timer_get_time() + CLK_MIN_TIME_US;

}


void TSL1401_ESP32::readSensorOnce() {

    readSensor();

    arrayCopying = true;
    for(uint8_t i = 0; i < SENSOR_ARRAY_LENGTH; i++) {
        pixelArr[i] = internalPixelArr[i];
    }
    arrayCopying = false;
}


/**
 * read the line sensor periodically in loop;
 * after the sensor is fully read the result is stored in pixelArr;
 * before accessing the pixelArr check if arrayCopying is false
 */
void TSL1401_ESP32::readSensorPeriodic() {

    digitalWrite(clkPin, LOW);
    lastFClkEdge = esp_timer_get_time();

    while(true) {
        readSensorOnce();
    }
}


void TSL1401_ESP32::readAverage(uint16_t cycles) {
    float c = (float) cycles;
    
    arrayCopying = true;
    for(uint8_t i = 0; i < SENSOR_ARRAY_LENGTH; i++) {
        averageArr[i] = 0;
    }

    for(uint16_t i=0; i < cycles; i++) {
        readSensor();
        
        for(uint8_t i = 0; i < SENSOR_ARRAY_LENGTH; i++) {
            averageArr[i] += internalPixelArr[i] / c;
        }
    }
    arrayCopying = false;
}

void TSL1401_ESP32::readAverageWithOffset(uint16_t offset, uint16_t cycles) {
    
    for(uint16_t i=0; i < offset; i++) {
        readSensor();
    }

    readAverage(cycles);
}