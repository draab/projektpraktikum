#ifndef tsl1401_h
#define tsl1401_h

#include <Arduino.h>

#define SENSOR_ARRAY_LENGTH 128

//#define SI_PIN GPIO_NUM_33 //serial input pin - SI - starts the exposure
// #define CLK_PIN GPIO_NUM_32 //clock pin - CLK // freq 5 - 2000 kHz
// #define AO_PIN GPIO_NUM_34 //analog output from TSL1401 - AO
// #define GND_PIN A2 //ground - GND -> for direct connection to arduino dev board
// #define VDD_PIN A0 //supply voltage - VDD -> for direct connection to arduino dev board

#define CLK_MIN_TIME_US 10    //t_w .. min 50ns
#define SI_MIN_SETUP_TIME_US 1 //t_su(SI) .. min 20ns
#define AO_SETTLING_TIME_US 1  // t_s ... 120ns to be +- 1%
#define INTEGRATION_MIN_TIME_US 1000 // t_int ... min 33.75us ... max 100000us
#define PIXEL_CHARGE_TRANSFER_TIME_US 50    //t_qt ... min 20us

class TSL1401_ESP32 {
    public:
        uint16_t pixelArr[SENSOR_ARRAY_LENGTH] = {0};
        float averageArr[SENSOR_ARRAY_LENGTH];
        void initPins(uint8_t clkPin, uint8_t siPin, uint8_t aoPin);
        void readSensorPeriodic();
        void readSensorOnce();
        void readAverage(uint16_t cycles);
        void readAverageWithOffset(uint16_t offset, uint16_t cycles);
        boolean volatile arrayCopying = false;

    private:
        uint8_t clkPin, siPin, aoPin;
        int64_t lastRClkEdge, lastFClkEdge, lastSiEdge, integratingStart = 0, lastReadClockCycle = 0, curTimerValue;
        uint16_t internalPixelArr[SENSOR_ARRAY_LENGTH] = {0};
        uint8_t curPixelIdx;

        void readSensor();
        void waitForTimerValue(int64_t timerValue);
};

#endif