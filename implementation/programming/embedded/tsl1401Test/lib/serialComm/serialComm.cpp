
#include "serialComm.h"

uint8_t initCommunication() {

    //pins will be defined for communication
    Serial.begin(SERIAL_BAUD_RATE);

    return 0;
}

uint8_t sendData(uint16_t dataArr[], uint16_t length) {

    Serial.println("tsl1401");

    for(int i=0; i< length-1; i++) {

      Serial.print(dataArr[i]);
      Serial.print(";");
    }
    Serial.println(dataArr[length-1]);
    Serial.flush();
    
    return 0;
}

bool isSendRequested() {

    if(Serial.available() > 1) {
        String message = Serial.readString();
        message.toLowerCase();
        if(message.startsWith("get")) {
            return true;
        }
    }

    return false;
}

