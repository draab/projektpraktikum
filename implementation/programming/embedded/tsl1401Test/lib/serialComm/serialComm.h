#ifndef serialcomm_h
#define serialcomm_h

#include <Arduino.h>

#ifndef SERIAL_BAUD_RATE
#define SERIAL_BAUD_RATE 256000
#endif

enum comm_mode {continuously, on_demand};

uint8_t initCommunication();
uint8_t sendData(uint16_t dataArr[], uint16_t length);
bool isSendRequested();

#endif