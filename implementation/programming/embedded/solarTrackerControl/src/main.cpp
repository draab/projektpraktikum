#include <Arduino.h>

#include "project_config.h"

#include "wifi_helper.h"
#include "influx_helper.h"
#include <tsl1401esp32.h>
#include <solarController.h>
#include <CheapStepper.h>
#include <my_ina219.h>
#include <OLEDController.h>

TSL1401_ESP32 tsl1401;
SolarController controller = SolarController(&tsl1401.averageArr[0], SENSOR_ARRAY_LENGTH);
CheapStepper stepper = CheapStepper(STEPPER_PIN_IN1, STEPPER_PIN_IN2, STEPPER_PIN_IN3, STEPPER_PIN_IN4);

OLEDController oled = OLEDController(I2C_PIN_OLED_SDA, I2C_PIN_OLED_SCL, &controller);
MyINA219 ina219;
float current = 0, voltage = 0;
uint8_t display_mode = 0;


/* #region serial output functions   */

uint8_t sendSerialDataFloat(float dataArr[], uint16_t length)
{

    Serial.println("tsl1401");

    for (int i = 0; i < length - 1; i++)
    {

        Serial.print(dataArr[i], 0);
        Serial.print(";");
    }
    Serial.println(dataArr[length - 1], 0);
    Serial.flush();

    return 0;
}

uint8_t sendSerialData(uint16_t dataArr[], uint16_t length)
{

    Serial.println("tsl1401");

    for (int i = 0; i < length - 1; i++)
    {

        Serial.print(dataArr[i]);
        Serial.print(";");
    }
    Serial.println(dataArr[length - 1]);
    Serial.flush();

    return 0;
}

/* #endregion */

void setup()
{
    Serial.begin(256000);

    wifi_setup();

    tsl1401.initPins(TSL1401_CLK_PIN, TSL1401_SI_PIN, TSL1401_AO_PIN);
    oled.init();
    stepper.setRpm(10);
    ina219.init();
}

void loop()
{

    tsl1401.calibrateSensor(10);

    tsl1401.readAverageWithOffset(10, 100);

    sendSerialDataFloat(tsl1401.averageArr, SENSOR_ARRAY_LENGTH);

    controller.calculateMinMax();
    controller.calculateMaxRegion(MAX_LEVEL_THRESHOLD_PERCENTAGE);

    ina219.readFromSensor();

    if(reconnectWLAN()) {
        influxdb_send_linesensor_metric(tsl1401, controller);
        influxdb_send_power_metric(ina219.loadvoltage_V, ina219.current_mA);
    }

    if (controller.getMax() < MIN_OF_MAX_VALUE)
    {
        // the incident light is too less 
        oled.displayMode = OLED_DISPLAY_MODE_ERROR_NO_PEAK;  
        if(reconnectWLAN()) influxdb_send_error_message("No sufficient peek found.");

        //TODO wait for more sun (night ?)

        oled.display(SLEEP_TIME_ON_ERROR_MS * 1000);
    }
    else if ((controller.getMax() - controller.getMin()) < MIN_SPAN_OF_MIN_TO_MAX_RANGE)
    {
        // the differenc between max and min incident light is to less
        oled.displayMode = OLED_DISPLAY_MODE_ERROR_LESS_DIFF;
        if(reconnectWLAN()) influxdb_send_error_message("To less light difference.");

        oled.display(SLEEP_TIME_ON_ERROR_MS * 1000);
    }
    else
    {
        // no error, display sensor data and check if movement is necessary
        oled.displayMode = OLED_DISPLAY_MODE_SENSOR_DATA_ONLY;
        // - oled.displayAllSensorData(controller);

        float angle2Rotate = controller.getAngleToRotate();
        float clockwise = true;
        if(angle2Rotate < 0) {
            angle2Rotate = -angle2Rotate;
            clockwise = false;
        }

        if (STEPPER_MOTOR_ENABLED && angle2Rotate > MIN_ANGLE_FOR_STARTING_ROTATION)
        {
            //stepper movement necessary
            stepper.moveDegrees(clockwise, angle2Rotate * GEAR_FACTOR);
            if(reconnectWLAN()) influxdb_send_stepper_metric(clockwise ? angle2Rotate : -angle2Rotate);
            oled.display(SLEEP_TIME_AFTER_STEPPER_MOVE_MS * 1000);
        }
        else
        {
            // no stepper movement necessary
            oled.display(SLEEP_TIME_ON_NO_MOVE_MS * 1000);
        }
    }


}