#include <OLEDController.h>

OLEDController::OLEDController(int16_t i2cSDA, int16_t i2cSCL, SolarController* controller) : oledDisplay(0x3c, i2cSDA, i2cSCL), solarController(controller) {}

void OLEDController::init() {
            
    // Initialising the UI will init the display too.
    oledDisplay.init();

    oledDisplay.flipScreenVertically();
    oledDisplay.setFont(ArialMT_Plain_10);
}

void OLEDController::drawSensorBar(uint16_t y, uint16_t value, uint16_t sensorArrayLength) {
    float ratio = ((float)OLED_DISPLAY_WIDTH) / sensorArrayLength;

    int16_t val = value * ratio;

    //border
    oledDisplay.setColor(WHITE);
    oledDisplay.drawRect(0, y, OLED_DISPLAY_WIDTH, OLED_SENSOR_BAR_HEIGHT);

    //value position
    oledDisplay.setColor(WHITE);
    oledDisplay.fillRect(val-1, y+1, 3, y + OLED_SENSOR_BAR_HEIGHT - 2);
    oledDisplay.setColor(BLACK);
    oledDisplay.drawLine(val, y+1, val, y + OLED_SENSOR_BAR_HEIGHT-2);

}

void OLEDController::drawSensorInfo(uint16_t y, float angle, float voltage_V, float current_mA) {
        // Text alignment demo
    oledDisplay.setFont(ArialMT_Plain_16);
    oledDisplay.setColor(WHITE);

    // The coordinates define the left starting point of the text - Diff
    oledDisplay.setTextAlignment(TEXT_ALIGN_LEFT);
    char buffer[20];
    sprintf(buffer, "%.1fV - %.1fmA", voltage_V, current_mA);
    oledDisplay.drawString(0, y, buffer);

    oledDisplay.setTextAlignment(TEXT_ALIGN_LEFT);
    char buffer2[10];
    sprintf(buffer2, "Diff:%.1f", angle);
    oledDisplay.drawString(0, y + 16, buffer2);
}

void OLEDController::branding() {
    oledDisplay.setFont(ArialMT_Plain_10);
    oledDisplay.setColor(WHITE);
    oledDisplay.setTextAlignment(TEXT_ALIGN_RIGHT);
    oledDisplay.drawString(OLED_DISPLAY_WIDTH, OLED_DISPLAY_HEIGHT-10, "by Daniel Raab");
}

void OLEDController::displayAllSensorData() {
    // clear the display
    oledDisplay.clear();


    drawSensorBar(0, solarController->getMaxMiddleIdx(), solarController->getSensorArrayLength());
    drawSensorInfo(OLED_SENSOR_BAR_HEIGHT, solarController->getAngleToRotate(), solarController->getMin(), solarController->getMax());
    branding();

    // write the buffer to the display
    oledDisplay.display();
}


void OLEDController::displayLineSensorLine(float* sensorArr, uint16_t sensorArrayLength) {
    // clear the display
    oledDisplay.clear();

    oledDisplay.setColor(WHITE);

    float scaleFactor = ((float)OLED_DISPLAY_HEIGHT) / 1023.0;
    float y = 0;

    for(uint16_t i=0; i<sensorArrayLength && i<OLED_DISPLAY_WIDTH; i++) {
        y = OLED_DISPLAY_HEIGHT - (*(sensorArr + i) * scaleFactor);
        oledDisplay.drawLine(i, y, i, y);   //draw a point
    }

    // write the buffer to the display
    oledDisplay.display();
}

void OLEDController::displayErrorMessage(String message) {
    // clear the display
    oledDisplay.clear();

    oledDisplay.setFont(ArialMT_Plain_16);
    oledDisplay.setColor(WHITE);
    oledDisplay.setTextAlignment(TEXT_ALIGN_LEFT);

    oledDisplay.drawString(0, 0, "Error:");

    oledDisplay.setTextAlignment(TEXT_ALIGN_CENTER);
    oledDisplay.drawStringMaxWidth(OLED_DISPLAY_WIDTH/2, 17, OLED_DISPLAY_WIDTH, message);


    // write the buffer to the display
    oledDisplay.display();
}


void OLEDController::display(int64_t time_us) {

    int64_t endTime = esp_timer_get_time() + time_us;
    uint8_t stepCnt = 0;
    int64_t stepTime = 0;

    while(endTime > esp_timer_get_time()) {
        if(stepCnt == 0) {
            stepTime = esp_timer_get_time() + OLED_DISPLAY_SHOW_DATA_TIME_US;
            stepCnt++;
        }
        else if(stepCnt == 1) {
            switch(displayMode) {
                case OLED_DISPLAY_MODE_SENSOR_DATA_ONLY:
                default:
                        displayAllSensorData();
                    break;
                case OLED_DISPLAY_MODE_ERROR_NO_PEAK:
                    displayErrorMessage("No sufficient peek found.");
                    break;

                case OLED_DISPLAY_MODE_ERROR_LESS_DIFF:
                    displayErrorMessage("To less light difference.");
                    break;
            }
            if(esp_timer_get_time() >= stepTime) {
                stepTime += OLED_DISPLAY_SHOW_SENSOR_LINE_TIME_US;
                stepCnt++;
            }
        }
        else if(stepCnt == 2) {
            displayLineSensorLine(solarController->getPixelArr(), solarController->getArrayLength());
            if(esp_timer_get_time() > stepTime) {
                stepCnt = 0;
            }
        }
    }
}