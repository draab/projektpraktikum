
#ifndef __OLEDController_h__
#define __OLEDController_h__

#include <Arduino.h>

// Include the correct display library
// For a connection via I2C using Wire include
//#include <Wire.h>  // Only needed for Arduino 1.6.5 and earlier
#include <SSD1306Wire.h> // legacy include: `#include "SSD1306.h"`
// or #include "SH1106Wire.h", legacy include: `#include "SH1106.h"`

#include <solarController.h>


#define OLED_DISPLAY_WIDTH 128
#define OLED_DISPLAY_HEIGHT 64

#define OLED_SENSOR_BAR_HEIGHT 15


#define OLED_DISPLAY_MODE_SENSOR_DATA_ONLY  0
#define OLED_DISPLAY_MODE_ERROR_NO_PEAK     1
#define OLED_DISPLAY_MODE_ERROR_LESS_DIFF   2

#define OLED_DISPLAY_SHOW_SENSOR_LINE_TIME_US 1000000
#define OLED_DISPLAY_SHOW_DATA_TIME_US 3000000



class OLEDController {
    private:
        // Initialize the OLED display using Wire library
        SSD1306Wire oledDisplay;
        SolarController* solarController;
        
    public:
        OLEDController(int16_t i2cSDA, int16_t i2cSCL, SolarController* controller);

        uint8_t displayMode = 0;

        void init();

        void displayLineSensorLine(float* sensorArr, uint16_t sensorArrayLength);

        // draws a bar with the given sensor value; value is the sensorArray Idx
        void drawSensorBar(uint16_t y, uint16_t value, uint16_t sensorArrayLength);

        // draws sensor values as string
        void drawSensorInfo(uint16_t y, float angle,  float voltage_V, float current_mA);

        // draws the string "by Daniel Raab" in the right lower corner
        void branding();

        // draws the full display
        void displayAllSensorData();

        void displayErrorMessage(String message);
        
        void display(int64_t time_us);
};


#endif