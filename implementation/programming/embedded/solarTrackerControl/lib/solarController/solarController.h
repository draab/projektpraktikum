#ifndef solar_controller_h
#define solar_controller_h

#include <stdint.h>
#include <limits.h>

class SolarController {
    public:
        SolarController(float pixelArr[], const uint16_t pixelLength);

        //getter functions
        float* getPixelArr();
        int32_t getArrayLength();
        float getMin();
        float getMax();
        int32_t getMinIdx();
        int32_t getMaxIdx();
        int32_t getMaxStartIdx();
        int32_t getMaxEndIdx();
        int32_t getMaxMiddleIdx();
        uint16_t getSensorArrayLength();

        //calculate min and max value and their position from pixel array
        void calculateMinMax();

        /*
         * calculate the region around the max value;
         * @param threshold value between 0 .. 1 (part of the min to max range the value is allowed to differ)
         */ 
        void calculateMaxRegion(float threshold);

        int32_t getMaxMiddleDiff();

        float getAngleToRotate();
    
    private:
        float *pixelArr;
        float min, max;
        int32_t arrLength, minIdx, maxIdx, maxStartIdx, maxEndIdx, maxMiddleIdx;
};


#endif //solar_controller_h