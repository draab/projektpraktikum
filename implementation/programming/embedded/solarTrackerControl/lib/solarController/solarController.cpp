
#include <solarController.h>

#include <Arduino.h>

SolarController::SolarController(float pixelArr[], const uint16_t arrLength) {
    this->pixelArr = pixelArr;
    this->arrLength = arrLength;
}

uint16_t SolarController::getSensorArrayLength() { return arrLength;}

void SolarController::calculateMinMax() {
    
    float val;
    min = 999999.9;
    max = 0;
    
    for(uint16_t i=0; i < arrLength; i++) {
        val = pixelArr[i];
        if(val < min)  {
            min = val;
            minIdx = i;
        }
        if(val > max) {
            max = val;
            maxIdx = i;
        }
    }
}

float* SolarController::getPixelArr() { return this->pixelArr; }
int32_t SolarController::getArrayLength() { return this->arrLength; }

float SolarController::getMin() { return min; }
float SolarController::getMax() { return max; }

int32_t SolarController::getMinIdx() { return minIdx; }
int32_t SolarController::getMaxIdx() { return maxIdx; }

/**
 * threshold in percentage 0..1
 */
void SolarController::calculateMaxRegion(float threshold) {
    float range = (max - min);

    float thresholdValue = max - (range * threshold);

    //take index of max and decrement the index while value is in threshold (threshold is a percentage value; 0.0 is min, 1.0 is max)
    for (maxStartIdx = maxIdx; maxStartIdx >= 0; maxStartIdx--)
    {
        if( pixelArr[maxStartIdx] < thresholdValue ) {
            maxStartIdx++;
            break;
        }
    }
    
    if(maxStartIdx < 0) maxStartIdx = 0;
    
    //do the same with incrementing the index
    for (maxEndIdx = maxIdx; maxEndIdx < arrLength ; maxEndIdx++)
    {
        if( pixelArr[maxEndIdx] < thresholdValue ) {
            maxEndIdx--;
            break;
        }
    }
    
    if(maxEndIdx >= arrLength) maxEndIdx = arrLength-1;

    // center of peak is the middle of the two founded indexs
    maxMiddleIdx = (maxStartIdx + maxEndIdx) / 2;
}


int32_t SolarController::getMaxStartIdx() { return maxStartIdx; }
int32_t SolarController::getMaxEndIdx() { return maxEndIdx; }
int32_t SolarController::getMaxMiddleIdx() { return maxMiddleIdx; }

int32_t SolarController::getMaxMiddleDiff() {
    return maxMiddleIdx - (arrLength/2);
}

float SolarController::getAngleToRotate() {
    return getMaxMiddleDiff() / 3;  //TODO
}