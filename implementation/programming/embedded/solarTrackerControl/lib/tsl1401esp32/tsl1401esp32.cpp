/**  
 * sensor tsl1401 (https://ams.com/linear-array)
 * datasheet: https://ams.com/documents/20143/36005/TSL1401CL_DS000136_3-00.pdf/8de4cae4-354c-c2c3-8db4-6a132f969a0a
 * 
 *    clk max 8 MHz
 * 
 * esp32:
 *    clk 240MHz -> 4.17ns per cycle
 *      -> digitalWrite: 30 cycles -> 120ns
 *      -> analogRead: 2400 cycles -> 10 us 
 * 
 * 
 * https://github.com/QuaireMerlinLouis/TSL1401_ESP32/blob/master/TSL1401_ESP32_dualcore.ino
 * 
 * -> https://github.com/empierre/arduino/blob/master/TSL1401
 */


#include "tsl1401esp32.h"

void TSL1401_ESP32::initPins(uint8_t clkPin, uint8_t siPin, uint8_t aoPin) {
    this->clkPin = clkPin;
    this->siPin = siPin;
    this->aoPin = aoPin;

    analogReadResolution(AD_RESOLUTION_BIT);   //set resolution of ADC to 10 bit
    
    //set pin modes
    pinMode(clkPin, OUTPUT);
    pinMode(siPin, OUTPUT);

    pinMode(aoPin, INPUT);

    // initialize CLK and SI pin
    digitalWrite(clkPin, LOW);
    digitalWrite(siPin, LOW);
    //set edge timestamps
    lastFClkEdge = esp_timer_get_time();
    lastSiEdge = lastFClkEdge;
    curPixelIdx = 0;
}


void TSL1401_ESP32::waitForTimerValue(int64_t timerValue) {
    do {
        //get timer value in every loop, until given value is reached
        curTimerValue = esp_timer_get_time();
    } while(curTimerValue < timerValue);
}


void TSL1401_ESP32::readSensor() {
    
    waitForTimerValue(lastReadClockCycle + PIXEL_CHARGE_TRANSFER_TIME_US);
    waitForTimerValue(integratingStart + integrationMinTimeUs);

    digitalWrite(siPin, HIGH);
    lastSiEdge = esp_timer_get_time();

    waitForTimerValue(lastSiEdge + SI_MIN_SETUP_TIME_US);
    waitForTimerValue(lastFClkEdge + CLK_MIN_TIME_US);
    digitalWrite(clkPin, HIGH);
    lastRClkEdge = esp_timer_get_time();
    curPixelIdx = 0;

    // no time to wait t_h(SI) is 0ns; SI must simply be low before next rising clk
    digitalWrite(siPin, LOW);

    waitForTimerValue(lastRClkEdge + CLK_MIN_TIME_US);
    digitalWrite(clkPin, LOW);
    lastFClkEdge = esp_timer_get_time();
    

    while(curPixelIdx < SENSOR_ARRAY_LENGTH) {

        waitForTimerValue(lastRClkEdge + AO_SETTLING_TIME_US);
        internalPixelArr[curPixelIdx] = analogRead(aoPin);      // analog value of pixel index is read

        waitForTimerValue(lastFClkEdge + CLK_MIN_TIME_US);
        digitalWrite(clkPin, HIGH);
        lastRClkEdge = esp_timer_get_time();
        curPixelIdx++;

        if(curPixelIdx == 18) {  // for measuring t_int
            integratingStart = lastRClkEdge;
        }

        waitForTimerValue(lastRClkEdge + CLK_MIN_TIME_US);
        digitalWrite(clkPin, LOW);
        lastFClkEdge = esp_timer_get_time();
    }

    //last read clock cycle is calculated; falling edge is not available here
    lastReadClockCycle = esp_timer_get_time() + CLK_MIN_TIME_US;

}


void TSL1401_ESP32::clockingWithoutSI(uint16_t count) {

    digitalWrite(clkPin, HIGH);
    lastRClkEdge = esp_timer_get_time();
    
    waitForTimerValue(lastRClkEdge + CLK_MIN_TIME_US);
    digitalWrite(clkPin, LOW);
    lastFClkEdge = esp_timer_get_time();

    while (count >= 0)
    {
        waitForTimerValue(lastFClkEdge + CLK_MIN_TIME_US);
        digitalWrite(clkPin, HIGH);
        lastRClkEdge = esp_timer_get_time();
        count--;

        waitForTimerValue(lastRClkEdge + CLK_MIN_TIME_US);
        digitalWrite(clkPin, LOW);
        lastFClkEdge = esp_timer_get_time();
    }
    
}


void TSL1401_ESP32::clockOneArrayWithoutRead() {

    digitalWrite(siPin, HIGH);
    lastSiEdge = esp_timer_get_time();
    
    digitalWrite(clkPin, HIGH);
    lastRClkEdge = esp_timer_get_time();
    curPixelIdx = 0;

    digitalWrite(siPin, LOW);

    waitForTimerValue(lastRClkEdge + CLK_MIN_TIME_US);
    digitalWrite(clkPin, LOW);
    lastFClkEdge = esp_timer_get_time();

    while(curPixelIdx < SENSOR_ARRAY_LENGTH) {
        waitForTimerValue(lastFClkEdge + CLK_MIN_TIME_US);
        digitalWrite(clkPin, HIGH);
        lastRClkEdge = esp_timer_get_time();
        curPixelIdx++;

        if(curPixelIdx == 18) {  // for measuring t_int
            integratingStart = lastRClkEdge;
        }

        waitForTimerValue(lastRClkEdge + CLK_MIN_TIME_US);
        digitalWrite(clkPin, LOW);
        lastFClkEdge = esp_timer_get_time();
    }

    //last read clock cycle is calculated; falling edge is not available here
    lastReadClockCycle = esp_timer_get_time() + CLK_MIN_TIME_US;
}


void TSL1401_ESP32::readSensorOnce() {

    clockOneArrayWithoutRead();
    readSensor();

    copyInternalToPublicArray();
}


void TSL1401_ESP32::readSensorPeriodic() {

    digitalWrite(clkPin, LOW);
    lastFClkEdge = esp_timer_get_time();

    while(true) {
        readSensorOnce();
    }
}


void TSL1401_ESP32::readAverage(uint16_t cycles) {
    float c = (float) cycles;
    
    //reset public pixelArr to zero
    arrayCopying = true;
    for(uint8_t i = 0; i < SENSOR_ARRAY_LENGTH; i++) {
        averageArr[i] = 0;
    }

    for(uint16_t i=0; i < cycles; i++) {
        clockOneArrayWithoutRead();
        readSensor();
        
        //copy read pixel array from an internal processing array to public readable pixelArr
        for(uint8_t j = 0; j < SENSOR_ARRAY_LENGTH; j++) {
            averageArr[j] += internalPixelArr[j] / c;
        }
    }
    arrayCopying = false;
}

void TSL1401_ESP32::readAverageWithOffset(uint16_t offset, uint16_t cycles) {
    
    for(uint16_t i=0; i < offset; i++) {
        clockOneArrayWithoutRead();
    }

    readAverage(cycles);
}


void TSL1401_ESP32::calibrateSensor(uint16_t maxCalibrationRuns) {

    //start with default integration factor or with last one
    if(integrationMinTimeUs == -1)
        integrationMinTimeUs = DEFAULT_INTEGRATION_MIN_TIME_US;


    for(; maxCalibrationRuns > 0; maxCalibrationRuns--) {

        //empty cycles to clean sensor integrator
        clockOneArrayWithoutRead();

        // read sensor
        readAverage(1);
        copyInternalToPublicArray();

        // find min and max
        SolarController c = SolarController(this->averageArr, SENSOR_ARRAY_LENGTH);
        c.calculateMinMax();

        //check if calibration in necessary
        float minDiff = CALIBRATION_THRESHOLD * AD_RESOLUTION_MAX_VALUE;    // minimum difference is calculated from A/D max resolution

        if(minDiff > (c.getMax() - c.getMin())) {
            // diff is small, calibration is necessary

            float middleOfSensor = (c.getMax() + c.getMin()) / 2;
            float diffToMiddle = middleOfSensor - (AD_RESOLUTION_MAX_VALUE / 2);

            //check if factor must be in- or decreased -> diffToMiddle is positiv if time must be decreased
            //adjust integration factor, calculate amount of adjustment
            float adjustingValue = (int64_t)(maxCalibrationRuns * -diffToMiddle);
            integrationMinTimeUs += adjustingValue;

            if(integrationMinTimeUs < 0) {
                integrationMinTimeUs = 0;
                break;
            }
            
        } else {
            break;  // no more calibration necessary, exit loop
        }
    }

    //try again -> loop ! max counter
}


int64_t TSL1401_ESP32::getIntegrationTimeMinUS() { return integrationMinTimeUs; }