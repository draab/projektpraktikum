#ifndef tsl1401_h
#define tsl1401_h

#include <Arduino.h>
#include <solarController.h>

#ifndef SENSOR_ARRAY_LENGTH
#define SENSOR_ARRAY_LENGTH 128     //neccessary for creating array
#endif

#ifndef AD_RESOLUTION_BIT
#define AD_RESOLUTION_BIT 10
#endif

#ifndef AD_RESOLUTION_MAX_VALUE
#define AD_RESOLUTION_MAX_VALUE 1023
#endif

#define CALIBRATION_THRESHOLD 0.3

//#define SI_PIN GPIO_NUM_33 //serial input pin - SI - starts the exposure
// #define CLK_PIN GPIO_NUM_32 //clock pin - CLK // freq 5 - 2000 kHz
// #define AO_PIN GPIO_NUM_34 //analog output from TSL1401 - AO
// #define GND_PIN A2 //ground - GND -> for direct connection to arduino dev board
// #define VDD_PIN A0 //supply voltage - VDD -> for direct connection to arduino dev board

#define CLK_MIN_TIME_US 1    //t_w .. min 50ns
#define SI_MIN_SETUP_TIME_US 1 //t_su(SI) .. min 20ns
#define AO_SETTLING_TIME_US 1  // t_s ... 120ns to be +- 1%
#define DEFAULT_INTEGRATION_MIN_TIME_US 30 // t_int ... min 33.75us ... max 100000us
#define PIXEL_CHARGE_TRANSFER_TIME_US 20    //t_qt ... min 20us

class TSL1401_ESP32 {
    public:
        /**
         * true if arraycopying (pixelArr or averageArr) is currently ongoing;
         * wait until value is false;
         * only in case of multithreading
         */
        boolean volatile arrayCopying = false;
        /**
         * contains the last fully readed PixelArray;
         * check arrayCopying before accessing !
         */
        uint16_t pixelArr[SENSOR_ARRAY_LENGTH] = {0};
        /**
         * containes averaged pixel values;
         * check arrayCopying before accessing !
         */
        float averageArr[SENSOR_ARRAY_LENGTH];


        /**
         * Init all pins for the tsl1401;
         * set ADC resolution to 10 bit
         */
        void initPins(uint8_t clkPin, uint8_t siPin, uint8_t aoPin);

        
        /**
         * read the line sensor periodically in loop;
         * after the sensor is fully read the result is stored in pixelArr
         */
        void readSensorPeriodic();

        /**
         * read the full sensor array and store the values to pixelArr
         */
        void readSensorOnce();


        /**
         * read 'cycles' count of cycles and build the average;
         * after all reads the average values are stored in averageArr;
         * the function does not support multithreading, read the array just after the function
         */
        void readAverage(uint16_t cycles);

        /**
         * like 'readAverage()' but the first 'offset' count of cycles are ignored;
         * after all 'cycles' reads the average values are stored in averageArr
         */
        void readAverageWithOffset(uint16_t offset, uint16_t cycles);

        /**
         * the method edits the value integrationMinTimeUs to fulfill the rule:
         * the min OR the max value must have a minimum threshold to the sensorlimits;
         * after <maxCalibrationRuns> the calibration is stopped
         */
        void calibrateSensor(uint16_t maxCalibrationRuns);

        int64_t getIntegrationTimeMinUS();

    private:
        // output- and input-pins for the sensor
        uint8_t clkPin, siPin, aoPin;

        //timer timestamps
        int64_t lastRClkEdge, lastFClkEdge, lastSiEdge, integratingStart = 0, lastReadClockCycle = 0, curTimerValue, integrationMinTimeUs = -1;

        //array for internal processing
        uint16_t internalPixelArr[SENSOR_ARRAY_LENGTH] = {0};
        //array for internal processing
        uint8_t curPixelIdx;

        /**
         * main sensor read function:
         * all clk cycles and si outputs are done here,
         * the analog reads are also done here
         */
        void readSensor();

        /**
         * clocks SENSOR_ARRAY_LENGTH + 1 times to clear the integrators in the sensor
         */
        void clockOneArrayWithoutRead();

        /**
         * 
         */
        void clockingWithoutSI(uint16_t count);

        /**
         * the function loops until the timer is greater or equal the given timerValue
         */
        void waitForTimerValue(int64_t timerValue);

        /**
         * copy read pixel array from an internal processing array to public readable pixelArr
         */
        void copyInternalToPublicArray() {
            arrayCopying = true;
            for(uint8_t i = 0; i < SENSOR_ARRAY_LENGTH; i++) {
                pixelArr[i] = internalPixelArr[i];
            }
            arrayCopying = false;
        }
};

#endif