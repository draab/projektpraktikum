
#ifndef influx_helper_h
#define influx_helper_h

#include <InfluxDbClient.h>
#include <solarController.h>
#include <tsl1401esp32.h>


// influx QL configs
#define INFLUXDB_URL "http://influxdb.draab.at:8086"
#define INFLUXDB_DB_NAME "photovoltaic"  // InfluxDB database name
#define INFLUXDB_TAG_NAME_LOCATION "location"

#define INFLUXDB_MEASUREMENT_LINE_SENSOR "lineSensor"
#define INFLUXDB_FIELD_PIXEL_VAL "val_"
#define INFLUXDB_FIELD_MAX_VALUE "maxValue"
#define INFLUXDB_FIELD_MIN_VALUE "minValue"
#define INFLUXDB_FIELD_PIXEL_MAX_BEGIN "maxBegin"
#define INFLUXDB_FIELD_PIXEL_MAX_END "maxEnd"
#define INFLUXDB_FIELD_PIXEL_MAX_MIDDLE "maxMiddle"
#define INFLUXDB_FIELD_INTEGRATION_TIME_US "TintUS"

#define INFLUXDB_MEASUREMENT_STEPPER "stepper"
#define INFLUXDB_FIELD_STEPPER_ANGLE "angle"

#define INFLUXDB_MEASUREMENT_POWER "power"
#define INFLUXDB_FIELD_POWER_VOLTAGE "voltage"
#define INFLUXDB_FIELD_POWER_CURRENT "current"

#define INFLUXDB_MEASUREMENT_ERROR "error"
#define INFLUXDB_FIELD_ERROR_MESSAGE "message"

// Single InfluxDB instance
InfluxDBClient client(INFLUXDB_URL, INFLUXDB_DB_NAME);


void influxdb_send_linesensor_metric(TSL1401_ESP32& sensor, SolarController controller) {
    // Define data point with sensor values
    Point sensorPoint(INFLUXDB_MEASUREMENT_LINE_SENSOR);
    // Set tags
    sensorPoint.addTag(INFLUXDB_TAG_NAME_LOCATION, "A-4722");

    // Add data
    char strBuffer[10];
    for(uint16_t i=0; i < SENSOR_ARRAY_LENGTH; i++) {
        snprintf(strBuffer, 10, "%s%d", INFLUXDB_FIELD_PIXEL_VAL, i);
        sensorPoint.addField(strBuffer, *(sensor.averageArr + i), 2);
    }
    sensorPoint.addField(INFLUXDB_FIELD_MAX_VALUE, controller.getMax());
    sensorPoint.addField(INFLUXDB_FIELD_MIN_VALUE, controller.getMin());
    sensorPoint.addField(INFLUXDB_FIELD_PIXEL_MAX_BEGIN, controller.getMaxStartIdx());
    sensorPoint.addField(INFLUXDB_FIELD_PIXEL_MAX_END, controller.getMaxEndIdx());
    sensorPoint.addField(INFLUXDB_FIELD_PIXEL_MAX_MIDDLE, controller.getMaxMiddleIdx());
    sensorPoint.addField(INFLUXDB_FIELD_INTEGRATION_TIME_US, ((int32_t) sensor.getIntegrationTimeMinUS()));

    // Write data
    client.writePoint(sensorPoint);

    //flush buffer
    if(client.flushBuffer() && client.getLastStatusCode() < 400) {
        Serial.println("Line sensor metric point submitted to influxdb");
    } else {
        Serial.print("Error in submitting point to influxdb");
        Serial.print(INFLUXDB_URL);
        Serial.print(" StatusCode: ");
        Serial.println(client.getLastStatusCode());
        Serial.println(client.getLastErrorMessage());
    }
}




void influxdb_send_stepper_metric(float angle) {
    // Define data point with sensor values
    Point sensorPoint(INFLUXDB_MEASUREMENT_STEPPER);
    // Set tags
    sensorPoint.addTag(INFLUXDB_TAG_NAME_LOCATION, "A-4722");

    // Add data
    sensorPoint.addField(INFLUXDB_FIELD_STEPPER_ANGLE, angle, 2);

    // Write data
    client.writePoint(sensorPoint);

    //flush buffer
    if(client.flushBuffer() && client.getLastStatusCode() < 400) {
        Serial.println("Stepper metric point submitted to influxdb");
    } else {
        Serial.print("Error in submitting point to influxdb");
        Serial.print(INFLUXDB_URL);
        Serial.print(" StatusCode: ");
        Serial.println(client.getLastStatusCode());
        Serial.println(client.getLastErrorMessage());
    }
}


void influxdb_send_power_metric(float voltage, float current) {
    // Define data point with sensor values
    Point sensorPoint(INFLUXDB_MEASUREMENT_POWER);
    // Set tags
    sensorPoint.addTag(INFLUXDB_TAG_NAME_LOCATION, "A-4722");

    // Add data
    sensorPoint.addField(INFLUXDB_FIELD_POWER_VOLTAGE, voltage, 2);
    sensorPoint.addField(INFLUXDB_FIELD_POWER_CURRENT, current, 2);

    // Write data
    client.writePoint(sensorPoint);

    //flush buffer
    if(client.flushBuffer() && client.getLastStatusCode() < 400) {
        Serial.println("Power metric point submitted to influxdb");
    } else {
        Serial.print("Error in submitting point to influxdb");
        Serial.print(INFLUXDB_URL);
        Serial.print(" StatusCode: ");
        Serial.println(client.getLastStatusCode());
        Serial.println(client.getLastErrorMessage());
    }
}



void influxdb_send_error_message(String message) {
    // Define data point with sensor values
    Point sensorPoint(INFLUXDB_MEASUREMENT_ERROR);
    // Set tags
    sensorPoint.addTag(INFLUXDB_TAG_NAME_LOCATION, "A-4722");

    // Add data
    sensorPoint.addField(INFLUXDB_FIELD_ERROR_MESSAGE, message);

    // Write data
    client.writePoint(sensorPoint);

    //flush buffer
    if(client.flushBuffer() && client.getLastStatusCode() < 400) {
        Serial.println("Error message point submitted to influxdb");
    } else {
        Serial.print("Error in submitting point to influxdb");
        Serial.print(INFLUXDB_URL);
        Serial.print(" StatusCode: ");
        Serial.println(client.getLastStatusCode());
        Serial.println(client.getLastErrorMessage());
    }
}

#endif