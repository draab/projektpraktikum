#ifndef __pinout_h__
#define __pinout_h__


//+++++++++++++  line sensor  tsl1401 ++++++++++++++

#define TSL1401_SI_PIN GPIO_NUM_33 //serial input pin - SI - starts the exposure
#define TSL1401_CLK_PIN GPIO_NUM_32 //clock pin - CLK // freq 5 - 2000 kHz
#define TSL1401_AO_PIN GPIO_NUM_34 //analog output from TSL1401 - AO




//+++++++++++++  stepper ++++++++++++++

#define STEPPER_PIN_IN1 GPIO_NUM_13
#define STEPPER_PIN_IN2 GPIO_NUM_12
#define STEPPER_PIN_IN3 GPIO_NUM_14
#define STEPPER_PIN_IN4 GPIO_NUM_27




//+++++++++++++ I2C Pins +++++++++++++
//INA219  I2C pins
#define I2C_PIN_INA219_SDA GPIO_NUM_21
#define I2C_PIN_INA219_SCL GPIO_NUM_22
//OLED I2C pins
#define I2C_PIN_OLED_SDA GPIO_NUM_21
#define I2C_PIN_OLED_SCL GPIO_NUM_22



#endif