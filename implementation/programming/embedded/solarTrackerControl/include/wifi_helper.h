
#ifndef __wifi_helper_h__
#define __wifi_helper_h__

#include <WiFiMulti.h>
WiFiMulti wifiMulti;

// WLAN settings
#define WLAN_WAIT_FOR_CONNECTION false
#define WLAN_SSID "WLANName"
#define WLAN_PASSWORD "WLANPassword"


/** This functions connects your ESP32 to your router.
 * If waitForConnection = false -> the function will do 120 loops (each 500ms) = 1min
 * if wl is not connected return false;
*/
boolean wifi_setup()
{
    // We start by connecting to a WiFi network
    Serial.println();
    Serial.print("Connecting to ");
    Serial.println(WLAN_SSID);
    WiFi.mode(WIFI_STA);
    wifiMulti.addAP(WLAN_SSID, WLAN_PASSWORD);
    uint8_t cntLoop = 120;
    while (wifiMulti.run() != WL_CONNECTED)
    {
        if (!WLAN_WAIT_FOR_CONNECTION)
        {
            if (cntLoop <= 0)
            {
                Serial.println("\nWiFi not connected - time limit reached.\n");
                return false;
            }
            cntLoop--;
        }
        delay(500);
        Serial.print(".");
    }
    Serial.println();
    Serial.print("WiFi connected - ESP IP address: ");
    Serial.println(WiFi.localIP());
    Serial.println();
    return true;
}

boolean reconnectWLAN() {
    // If no Wifi signal, try to reconnect it
    if (wifiMulti.run() != WL_CONNECTED) {
        Serial.println("Wifi connection lost");
        return false;
    }
    return true;
}

void wifi_disconnect()
{
    Serial.println("Disconnect from Wifi.");
    WiFi.disconnect();
}

#endif //__