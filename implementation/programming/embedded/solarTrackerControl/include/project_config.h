#ifndef __project_configs_h__
#define __project_configs_h__

#include "pinout.h"

#define SERIAL_BAUD_RATE 256000

#define SENSOR_ARRAY_LENGTH 128

#define STEPPER_MOTOR_ENABLED true

// the factor calculated from the maximum value of the sensor for finding the range of illumination
#define MAX_LEVEL_THRESHOLD_PERCENTAGE 0.35

// the minimum range between minimum and maximum of the sensor values (otherwise no stepper movment, eg. night, cloudy) (on 1023 range)
#define MIN_SPAN_OF_MIN_TO_MAX_RANGE 100

// the minimum, the difference between actual sun position and sensor middle position must have (for stepper movment) 
#define MIN_ANGLE_FOR_STARTING_ROTATION 2.0

// the minimum of the highest occuring value from the sensor must have (otherwise no stepper movment, eg. night, very cloudy)
#define MIN_OF_MAX_VALUE 50


// time to sleep (in millisec.) if an error occurred
#define SLEEP_TIME_ON_ERROR_MS 60000

// time to sleep (in millisec.) if a movment of the stepper was necessary
#define SLEEP_TIME_AFTER_STEPPER_MOVE_MS 10000

// time to sleep (in millisec.) if no movement was necessary and no error occurred
#define SLEEP_TIME_ON_NO_MOVE_MS 300000

// gear factor for the gear (gear tooth count of base / gear tooth count of the gearwheel on the stepper motor)
#define GEAR_FACTOR 100.0/9.0




#endif // !__project_configs_h__