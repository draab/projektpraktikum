﻿using System;
using System.IO.Ports;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using System.Net.Security;

namespace PortReader
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const int CNT_FPS_CALC = 5;
        bool _continueReading = true;
        SensorDataReader _sensorDataReader;
        SensorDataReader.SensorData _sensorData = SensorDataReader.SensorDataIntArray.EMPTY;
        long _sensorDataCnt = 0;
        long _lastTimeStamp;
        float _fps;
        Thread readThread;

        public MainWindow()
        {
            InitializeComponent();

            updatePortBox();

            //todo remove
            //dummyFillArray();
        }

        private void updatePortBox()
        {
            //delete old once
            portBox.Items.Clear();


            // Display each port name to the console.
            foreach (string port in SensorDataReader.GetPortNameArray())
            {
                portBox.Items.Add(port);
            }
        }

        private void refreshPortsBtn_Click(object sender, RoutedEventArgs e)
        {
            updatePortBox();

            //TODO remove
            //SensorDataHasUpdated(0);
        }

        private void dummyFillArray()
        {
            _sensorData = new SensorDataReader.SensorDataIntArray(128);

            for (int i = 0; i < (((SensorDataReader.SensorDataIntArray)_sensorData).GetDataArray().Length) ; i++) {
                if(i <40)
                ((SensorDataReader.SensorDataIntArray)_sensorData).SetArrayIndex(i, i+10);
                else if(i<100)
                    ((SensorDataReader.SensorDataIntArray)_sensorData).SetArrayIndex(i, -i + 200);
                else
                    ((SensorDataReader.SensorDataIntArray)_sensorData).SetArrayIndex(i, 5*i + 10);
            }
        }


        private void SensorDataHasUpdated()
        {
            updateColorLine();
            updateDiagramPolygon();
            updateLabels();
        }

        private void updateLabels()
        {
            receivedLabel.Content = "Received: " + _sensorDataCnt;
            fpsLabel.Content = String.Format("Fps: {0:0.00}", _fps);
            maxLabel.Content = "Max: " + ((SensorDataReader.SensorDataIntArray)_sensorData).Max;
            minLabel.Content = "Min: " + ((SensorDataReader.SensorDataIntArray)_sensorData).Min;

            maxLabel.Content = "Max: " + ((SensorDataReader.SensorDataIntArray)_sensorData).Max;
            minLabel.Content = "Min: " + ((SensorDataReader.SensorDataIntArray)_sensorData).Min;
        }

        private void updateColorLine()
        {
            //remove all color blocks
            colorLine.Children.Clear();

            int[] dataArr = ((SensorDataReader.SensorDataIntArray)_sensorData).GetDataArray();


            if (dataArr.Length > 0)
            {
                double dataStepWidth = diagram.ActualWidth / dataArr.Length;
                double sensorRange = (SensorDataReader.MAX_SENSOR_VALUE - SensorDataReader.MIN_SENSOR_VALUE);

                for (int i = 0; i < dataArr.Length; i++)
                {
                    Rectangle r = new Rectangle();
                    r.StrokeThickness = 0;
                    r.Stroke = System.Windows.Media.Brushes.Transparent;
                    r.Width = dataStepWidth;
                    r.Height = colorLine.ActualHeight;
                    byte cValue = (byte) (255 *(1- (dataArr[i] / sensorRange)));
                    SolidColorBrush mySolidColorBrush = new SolidColorBrush();
                    mySolidColorBrush.Color = Color.FromRgb(cValue, cValue, cValue);
                    r.Fill = mySolidColorBrush;

                    colorLine.Children.Add(r);
                    Canvas.SetTop(r, 0);
                    Canvas.SetLeft(r, i * dataStepWidth);

                }

            }

        }

        private void updateDiagramPolygon()
        {
            //remove all lines
            diagram.Children.Clear();

            int[] dataArr = ((SensorDataReader.SensorDataIntArray)_sensorData).GetDataArray();

            if (dataArr.Length > 0)
            {
                double dataStepWidth = diagram.ActualWidth / (dataArr.Length - 1);
                double sensorRange = (SensorDataReader.MAX_SENSOR_VALUE - SensorDataReader.MIN_SENSOR_VALUE);

                Polygon diagramPoly = new Polygon();
                diagramPoly.Stroke = System.Windows.Media.Brushes.Black;
                diagramPoly.StrokeThickness = 1;
                diagramPoly.Fill = System.Windows.Media.Brushes.LightGreen;

                PointCollection pc = new PointCollection();

                for (int i = 0; i < dataArr.Length; i++)
                {
                    Point p = new Point(i * dataStepWidth, diagram.ActualHeight * (1 - (dataArr[i] / sensorRange)));
                    pc.Add(p);
                }

                pc.Add(new Point(diagram.ActualWidth, diagram.ActualHeight));
                pc.Add(new Point(0, diagram.ActualHeight));

                diagramPoly.Points = pc;
                diagram.Children.Add(diagramPoly);



                if(((SensorDataReader.SensorDataIntArray)_sensorData).MaxRangeSet)
                {
                    Line startLine = new Line();
                    startLine.X1 = startLine.X2 = ((SensorDataReader.SensorDataIntArray)_sensorData).MaxStartIdx * dataStepWidth;
                    startLine.Y1 = 0;
                    startLine.Y2 = diagram.ActualHeight;
                    startLine.StrokeThickness = 1;
                    startLine.Stroke = System.Windows.Media.Brushes.DarkRed;
                    diagram.Children.Add(startLine);

                    Line endLine = new Line();
                    endLine.X1 = endLine.X2 = (((SensorDataReader.SensorDataIntArray)_sensorData).MaxEndIdx + 1) * dataStepWidth;
                    endLine.Y1 = 0;
                    endLine.Y2 = diagram.ActualHeight;
                    endLine.StrokeThickness = 1;
                    endLine.Stroke = System.Windows.Media.Brushes.DarkRed;
                    diagram.Children.Add(endLine);
                }
            }
        }


        private void updateDiagramSingelLines()
        {
            //remove all lines
            diagram.Children.Clear();

            int[] dataArr = ((SensorDataReader.SensorDataIntArray)_sensorData).GetDataArray();

            if (dataArr != null && dataArr.Length > 1) {

                double sensorRange = (SensorDataReader.MAX_SENSOR_VALUE - SensorDataReader.MIN_SENSOR_VALUE);

                int lastValue = dataArr[0];
                double dataStepWidth = diagram.ActualWidth / (dataArr.Length-1);

                for (int i = 1; i < dataArr.Length; i++)
                {
                    Line test = new Line();
                    test.Stroke = System.Windows.Media.Brushes.Black;

                    test.X1 = (i-1)* dataStepWidth;
                    test.X2 = i* dataStepWidth;
                    test.Y1 = diagram.ActualHeight * (1 - (dataArr[i-1]/sensorRange));
                    test.Y2 = diagram.ActualHeight * (1- (dataArr[i] / sensorRange));
                    test.HorizontalAlignment = HorizontalAlignment.Left;
                    test.VerticalAlignment = VerticalAlignment.Center;
                    test.StrokeThickness = 1;
                    diagram.Children.Add(test);

                    lastValue = dataArr[i];
                }

            }
        }



        private void connectBtn_Click(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("connectBtn_Click");
            readThread = new Thread(UpdateData);

            try
            {
                _sensorDataReader = SensorDataReader.ConnectToPort(GetPortName(), GetPortBaudRate());

                _lastTimeStamp = DateTimeOffset.Now.ToUnixTimeMilliseconds();

                _continueReading = true;
                readThread.Start();

            } catch (Exception exc)
            {
                Console.WriteLine(exc.Message);
            }
        }

        /// <summary>
        /// thread function for updating data
        /// </summary>
        public void UpdateData()
        {
            while (_continueReading)
            {
                try
                {
                    _sensorData = _sensorDataReader.ReadSingleDataStream();
                    _sensorDataCnt++;
                    if(_sensorDataCnt % CNT_FPS_CALC == 0)
                    {
                        long now = DateTimeOffset.Now.ToUnixTimeMilliseconds();
                        _fps = (CNT_FPS_CALC * 1000f) / (now - _lastTimeStamp);
                        _lastTimeStamp = now;
                    }
                    this.Dispatcher.Invoke(() => { SensorDataHasUpdated(); });
                }
                catch (TimeoutException) { }
                catch (Exception e) {
                    Console.Out.WriteLine(e.Message);
                }
            }
        }

        private void DisconnectSerialPort()
        {
            _continueReading = false;
            _sensorDataCnt = 0;
            _sensorData = SensorDataReader.SensorDataIntArray.EMPTY;
            //try { readThread.Abort();  }
            //catch (Exception) { }
            try { _sensorDataReader.ClosePort(); }
            catch (Exception) { }

            SensorDataHasUpdated();
        }


        /// <summary>
        /// reads port name from text field
        /// </summary>
        /// <param name="defaultPortName"></param>
        /// <returns></returns>
        public string GetPortName(string defaultPortName="COM1")
        {
            string portName;
            portName = (string)portBox.SelectedItem;

            if (portName == null || portName == "" || !(portName.ToLower()).StartsWith("com"))
            {
                portName = defaultPortName;
            }
            return portName;
        }

        /// <summary>
        /// reads baut from input text view and parses to int
        /// </summary>
        /// <param name="defaultPortBaudRate"></param>
        /// <returns></returns>
        public int GetPortBaudRate(int defaultPortBaudRate=115200)
        {
            string baudRate;
            baudRate = bautText.Text;

            if (baudRate == "")
            {
                baudRate = defaultPortBaudRate.ToString();
            }

            return int.Parse(baudRate);
        }
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            DisconnectSerialPort();
        }

        private void disconnectBtn_Click(object sender, RoutedEventArgs e)
        {
            DisconnectSerialPort();
        }
    }
}
