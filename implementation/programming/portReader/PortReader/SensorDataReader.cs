﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortReader
{
    class SensorDataReader
    {

        public const int SENSOR_ARRAY_LENGTH = 128;
        public const int MIN_SENSOR_VALUE = 0;
        public const int MAX_SENSOR_VALUE = 1023;
        private SerialPort _serialPort;



        /// <summary>
        /// Get a list of serial port names.
        /// </summary>
        public static string[] GetPortNameArray()
        {
            return SerialPort.GetPortNames();
        }


        /// <summary>
        /// creates a new SensorDataReader object from given port and baut
        /// </summary>
        /// <returns></returns>
        public static SensorDataReader ConnectToPort(string port, int baut)
        {
            SerialPort _serialPort = new SerialPort();
            _serialPort.PortName = port;
            _serialPort.BaudRate = baut;


            //_serialPort.Parity = SetPortParity(_serialPort.Parity);
            //_serialPort.DataBits = SetPortDataBits(_serialPort.DataBits);
            //_serialPort.StopBits = SetPortStopBits(_serialPort.StopBits);
            //_serialPort.Handshake = SetPortHandshake(_serialPort.Handshake);

            // Set the read/write timeouts
            _serialPort.ReadTimeout = 500;
            _serialPort.WriteTimeout = 500;

            _serialPort.Open();

            return new SensorDataReader(_serialPort);
        }

        private SensorDataReader(SerialPort serialPort)
        {
            this._serialPort = serialPort;
        }


        /// <summary>
        /// message hast do be the format:
        /// tsl1401
        /// val0;val1;val2;val3....
        /// </summary>
        /// <returns></returns>
        public SensorData ReadSingleDataStream()
        {
            string message = _serialPort.ReadLine();

            if(message.StartsWith("tsl1401"))
            {
                SensorDataIntArray sd = new SensorDataIntArray(SENSOR_ARRAY_LENGTH);

                string valueString = _serialPort.ReadLine();

                if (valueString.StartsWith("maxStartIdx:"))
                {
                    sd.MaxStartIdx = int.Parse(valueString.Substring(12));

                    valueString = _serialPort.ReadLine();
                    if (valueString.StartsWith("maxEndIdx:"))
                    {
                        sd.MaxEndIdx = int.Parse(valueString.Substring(10));
                        sd.MaxRangeSet = true;

                        valueString = _serialPort.ReadLine();
                    }
                }


                string[] valueArr = valueString.Split(';');
                if (valueArr.Length != SENSOR_ARRAY_LENGTH)
                    Console.WriteLine("Delivered sensor data array length is not matching. Given %d; should be %d", valueArr.Length, SENSOR_ARRAY_LENGTH);

                for (int i=0; i<valueArr.Length; i++)
                {
                    sd.SetArrayIndex(i, int.Parse(valueArr[i]));
                }

                // _serialPort.WriteLine("OK");

                return sd;
            } 
            else
            {
                throw new Exception("message not known: "+message);
            }

        }


        public void ClosePort()
        {
            if(_serialPort != null)
            {
                try
                {
                    _serialPort.Close();
                } catch(Exception) { }
            }
        }



        public interface SensorData
        {}

        public class SensorDataIntArray : SensorData
        {
            public static readonly SensorDataIntArray EMPTY;
            static SensorDataIntArray() {
                EMPTY = new SensorDataIntArray(0);
            }

            private int[] _dataArr;
            private Boolean _maxRangeSet = false;

            public int Min { get; internal set; }
            public int Max { get; internal set; }
            public Boolean MaxRangeSet
            {
                get { return _maxRangeSet; }
                set { _maxRangeSet = value; }
            }
            public int MaxStartIdx { get; set; }
            public int MaxEndIdx { get; set; }

            public SensorDataIntArray(int arrayLength)
            {
                _dataArr = new int[arrayLength];
                Min = int.MaxValue;
                Max = int.MinValue;
            }

            public bool SetArrayIndex(int index, int data)
            {
                if(index < _dataArr.Length)
                {
                    if (data > Max)
                        Max = data;
                    if (data < Min)
                        Min = data;
                    _dataArr[index] = data;
                    return true;
                }
                return false;
            }


            public int[] GetDataArray()
            {
                return _dataArr;
            }
        }
    }
}
