# Bachelorprojekt

Computer vision with line sensor TSL1401 - Raab Daniel

Supervisor: JKU - [Department of Computational Perception](http://www.cp.jku.at/) - Scharinger Josef

## Sensor:
* TSL1401
  * Home https://ams.com/tsl1401cl#tab/applications
  * Datasheet https://ams.com/documents/20143/36005/TSL1401CL_DS000136_3-00.pdf
    * alternative: http://www.w-r-e.de/robotik/data/tsl1401.pdf
* Basisframework zum Auslesen vorhanden.
* welche Alternativen gibt es zum vorhandenen Senor
  * Preisliste Lineare Bildsensoren https://www.sander-electronic.de/be00010.html
## Literatursuche

Datenbank Infosystem: https://dbis.uni-regensburg.de/dbinfo/fachliste.php?bib_id=ubli
Google Scholar: https://scholar.google.com/

* Welche Anwendungsfälle gibt es.
  * **peak follower for photovoltaik**
  * https://ams.com/tsl1401cl#tab/applications:
    * printer edge detection
    * scanners
    * optical character recognition
    * position sensing
* Kalibrierung

### Mögliche Literatur:

* Method and apparatus for dispensing a liquid into a receptacle https://patents.google.com/patent/US6100518A/en
  https://patentimages.storage.googleapis.com/ac/7a/46/b7f163a1b33068/US6100518.pdf
* Spektroskop - https://www.instructables.com/Arduino-Spectroscope-With-TSL1401-and-Display/
  * Forum - https://forum.mosfetkiller.de/viewtopic.php?f=3&t=63852
  * Video - https://youtu.be/K6gvthSJu2I
* Video - Line follower robot - https://youtu.be/DRwYoauohxY
* Using Parallax TSL1401-DB Linescan Camera Module for line detection http://libio.izt.uam.mx/Downloads/kl25z/controla%20camara/camara.pdf

### verwendbare Literatur (zusätzlich zur /Literatur/_readme.md):

* PC-Based Line-Scan Imaging Systems
  * Maggie Huang
  * https://www.adlinktech.com/solution/measure/20050926.htm
* good introduction and enumeration **but no further description of practical use cases**
  https://arduining.com/2014/03/26/using-the-linear-sensor-array-tsl201r-with-arduino/
  * *Line Follower Robots*
  * Optical Flow Sensor (to sense movement in robots and autonomous vehicles)
  * Laser Range Finders.
  * Imagen Scanners (Barcode, character recognition etc.)
  * Position Sensors.
  * Object Detection and Object Counting
  * Object Shape Identification and Analysis
  * Light Spectrum Analyzers.

### Links ohne nähere Beschreibung:

* https://www.robotics.org.za/TSL1401: **All further links on this site are down**
  * Linear Sensor Array Optical Path Design
  * Position Detection Using Maximal Length Sequences
  * SELFOC® Lens Arrays for Line Scanning Applications
  * TSL14xx Series Power Up Sequencing
  * Using the TSL3301 with a Microcontroller
  * TAOS Photo Sensor Response Part I: Sensitivity to Wavelength
  * TAOS Photo Sensor Response Part II: Sensitivity to Temperature
  * Compensating for Light Flicker on Optical Sensors