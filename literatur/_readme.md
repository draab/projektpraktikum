# Literaturverzeichnis

## Callibration

* Intrinsic Parameter Calibration of Line-Scan Cameras Using RANSAC Algorithm

  * Zheng Zhu; Qunfang Xiong; Jintao Chen; Feng Zhang; Xing Liu; Guangde Yao

  * https://ieeexplore-1ieee-1org-1llq4z3h202ac.han.ubl.jku.at/document/8666916

  * ```
    @INPROCEEDINGS{8666916,
    author={Z. {Zhu} and Q. {Xiong} and J. {Chen} and F. {Zhang} and X. {Liu} and G. {Yao}},
    booktitle={2018 International Conference on Information Systems and Computer Aided Education (ICISCAE)}, 
    title={Intrinsic Parameter Calibration of Line-Scan Cameras Using RANSAC Algorithm}, 
    year={2018},
    volume={},
    number={},
    pages={416-425},
    doi={10.1109/ICISCAE.2018.8666916}}
    ```

* The Line Scan Camera Calibration Based on Space Rings Group

  * Menghui Niu; Kechen Song; Xin Wen; Defu Zhang; Yunhui Yan

  * https://ieeexplore-1ieee-1org-1llq4z3h202ac.han.ubl.jku.at/document/8320770

  * ```
    @ARTICLE{8320770,
    author={M. {Niu} and K. {Song} and X. {Wen} and D. {Zhang} and Y. {Yan}},
    journal={IEEE Access}, 
    title={The Line Scan Camera Calibration Based on Space Rings Group}, 
    year={2018},
    volume={6},
    number={},
    pages={23711-23721},
    doi={10.1109/ACCESS.2018.2817629}}
    ```

* A New Calibration Method of Line Scan Camera for High-Precision Two-Dimensional Measurement

  * Jiabin Zhang; Zhengtao Zhang; Fei Shen; Feng Zhang; Hu Su

  * https://ieeexplore-1ieee-1org-1llq4z3h202ac.han.ubl.jku.at/document/8560438

  * ```
    @INPROCEEDINGS{8560438,
    author={J. {Zhang} and Z. {Zhang} and F. {Shen} and F. {Zhang} and H. {Su}},
    booktitle={2018 IEEE 14th International Conference on Automation Science and Engineering (CASE)}, 
    title={A New Calibration Method of Line Scan Camera for High-Precision Two-Dimensional Measurement}, 
    year={2018},
    volume={},
    number={},
    pages={678-683},
    doi={10.1109/COASE.2018.8560438}}
    ```

* Calibration of Line-Scan Cameras

  * Carlos A. Luna, Manuel Mazo, José Luis Lázaro, Juan F. Vázquez

  * https://ieeexplore-1ieee-1org-1llq4z3h202ac.han.ubl.jku.at/document/5286844

  * ```latex
    @ARTICLE{5286844,
    author={C. A. {Luna} and M. {Mazo} and J. L. {Lazaro} and J. F. {Vazquez}},
    journal={IEEE Transactions on Instrumentation and Measurement}, 
    title={Calibration of Line-Scan Cameras}, 
    year={2010},
    volume={59},
    number={8},
    pages={2185-2190},
    doi={10.1109/TIM.2009.2031344}}
    ```

* A Novel Line Scan Camera Calibration Technique With an Auxiliary Frame Camera

  * [Bingwei Hui](https://ieeexplore-1ieee-1org-1llq4z3h202ac.han.ubl.jku.at/author/37987685300); [Gongjian Wen](https://ieeexplore-1ieee-1org-1llq4z3h202ac.han.ubl.jku.at/author/37840641100); [Peng Zhang](https://ieeexplore-1ieee-1org-1llq4z3h202ac.han.ubl.jku.at/author/37072812400); [Deren Li](https://ieeexplore-1ieee-1org-1llq4z3h202ac.han.ubl.jku.at/author/37277110100)

  * https://ieeexplore-1ieee-1org-1llq4z3h202ac.han.ubl.jku.at/document/6563200

  * ```latex
    @ARTICLE{6563200,
    author={B. {Hui} and G. {Wen} and P. {Zhang} and D. {Li}},
    journal={IEEE Transactions on Instrumentation and Measurement}, 
    title={A Novel Line Scan Camera Calibration Technique With an Auxiliary Frame Camera}, 
    year={2013},
    volume={62},
    number={9},
    pages={2567-2575},
    doi={10.1109/TIM.2013.2256815}}
    ```

## Applications

### car

#### line follower / steering control

* A development of a line-trace car with fuzzy control of motor signals and performance evaluation

  * [Yuka Nishiyama](https://ieeexplore-1ieee-1org-1llq4z3h202ac.han.ubl.jku.at/author/37086837703); [Yuki Shinomiya](https://ieeexplore-1ieee-1org-1llq4z3h202ac.han.ubl.jku.at/author/37086836727); [Toshimi Yamamoto](https://ieeexplore-1ieee-1org-1llq4z3h202ac.han.ubl.jku.at/author/37086836756); [Yukinobu Hoshino](https://ieeexplore-1ieee-1org-1llq4z3h202ac.han.ubl.jku.at/author/37086837081)
  * https://ieeexplore-1ieee-1org-1llq4z3h202ac.han.ubl.jku.at/document/8716049
  * 

  ```
  @INPROCEEDINGS{8716049,
  author={Y. {Nishiyama} and Y. {Shinomiya} and T. {Yamamoto} and Y. {Hoshino}},
  booktitle={2018 Joint 10th International Conference on Soft Computing and Intelligent Systems (SCIS) and 19th International Symposium on Advanced Intelligent Systems (ISIS)}, 
  title={A Development of a Line-Trace Car with Fuzzy Control of Motor Signals and Performance Evaluation}, 
  year={2018},
  volume={},
  number={},
  pages={758-761},
  doi={10.1109/SCIS-ISIS.2018.00126}}
  ```

* STEERING CONTROL  METHOD BASED ON TSL1401 LINEAR SENSOR ARRAY

  * Mohamad Taib Miskon , Ahmad Shahran , Ibrahim , Zairi Ismael Rizman , Nuraiza Ismail

  * http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.1057.8837

  * ```latex
    @MISC{Miskon_steeringcontrol,
        author = {Mohamad Taib Miskon and Ahmad Shahran and Ibrahim and Zairi Ismael Rizman and Nuraiza Ismail},
        title = {STEERING CONTROL METHOD BASED ON TSL1401 LINEAR SENSOR ARRAY},
        year = {}
    }
    ```

#### car detection

* Street-parking vehicle detection using line scan camera

  * C.H. Zhu; K. Hirahara; K. Ikeuchi

  * https://ieeexplore-1ieee-1org-1llq4z3h202ac.han.ubl.jku.at/document/1212976

  * ```
    @INPROCEEDINGS{1212976,
    author={C. H. {Zhu} and K. {Hirahara} and K. {Ikeuchi}},
    booktitle={IEEE IV2003 Intelligent Vehicles Symposium. Proceedings (Cat. No.03TH8683)}, 
    title={Street-parking vehicle detection using line scan camera}, 
    year={2003},
    volume={},
    number={},
    pages={575-580},
    doi={10.1109/IVS.2003.1212976}}
    ```
    

* **Double to previous:** Detection of street-parking vehicles using line scan camera and scanning laser range sensor

  * K. Hirahara; K. Ikeuchi

  * https://ieeexplore-1ieee-1org-1llq4z3h202ac.han.ubl.jku.at/document/1212990

  * ```
    @INPROCEEDINGS{1212990,
    author={K. {Hirahara} and K. {Ikeuchi}},
    booktitle={IEEE IV2003 Intelligent Vehicles Symposium. Proceedings (Cat. No.03TH8683)},
    title={Detection of street-parking vehicles using line scan camera and scanning laser range sensor}, 
    year={2003},
    volume={},
    number={},
    pages={656-661},
    doi={10.1109/IVS.2003.1212990}}
    ```


* High accuracy traffic monitoring using road-side line-scan cameras

  * D. Douxchamps, B. Macq and K. Chihara

  * https://ieeexplore-1ieee-1org-1llq4z3h202ac.han.ubl.jku.at/document/1706854

  * ```
    @INPROCEEDINGS{1706854,
    author={D. {Douxchamps} and B. {Macq} and K. {Chihara}},
    booktitle={2006 IEEE Intelligent Transportation Systems Conference}, 
    title={High accuracy traffic monitoring using road-side line-scan cameras}, 
    year={2006},
    volume={},
    number={},
    pages={875-878},
    doi={10.1109/ITSC.2006.1706854}}
    ```

### Sun / PV

* Sun Altitude Sensor

  * Todor Stoyanov Djamiykov

  * http://ecad.tu-sofia.bg/et/2014/ET2014/AJE_2014/013-T_Djamiykov.pdf

  * ``` latex
    @article{djamiykovsun,
    title={Sun altitude Sensor},
    author={Djamiykov, T and Alexiev, D and Iovev, A and Marinov, M}
    }
    ```

* no - Sensor array for PV shading measurements

  * DOI: 10.1109/ICSENS.2011.6127368

  * https://ieeexplore-1ieee-1org-1llq4z32e0004.han.ubl.jku.at/document/6127368

  * ```latex
    @INPROCEEDINGS{6127368,
    author={C. D. {Barreiro} and A. {Bross} and J. L. {Schmalzel} and P. M. {Jansson}},
    booktitle={SENSORS, 2011 IEEE}, 
    title={Sensor array for PV shading measurements}, 
    year={2011},
    volume={},
    number={},
    pages={1889-1892},
    doi={10.1109/ICSENS.2011.6127368}}
    ```

### others

* Turbidity sensor for underwater applications (Sensor Design and System Performance with Calibration Results)

  * Saba Mylvaganam , Torgeir Jakobsen 'Hogskolen i Bergen (Bergen College), P. 0. Box 6030, N-5020 Bergen 2Torgeir Jakobsen, Aanderaa Instruments, Fanavegen  13 B, N-505 1 Bergen 

  * ```latex
    @inproceedings{mylvaganaru1998turbidity,
      title={Turbidity sensor for underwater applications},
      author={Mylvaganaru, S and Jakobsen, Torgeir},
      booktitle={IEEE Oceanic Engineering Society. OCEANS'98. Conference Proceedings (Cat. No. 98CH36259)},
      volume={1},
      pages={158--161},
      year={1998},
      organization={IEEE}
    }
    ```

* ??? - Position detection using maximal length sequences

  * https://docplayer.net/50089236-Position-detection-using-maximal-length-sequences.html

* PC-Based Line-Scan Imaging Systems

  * Maggie Huang
  
* https://www.adlinktech.com/solution/measure/20050926.htm
  
* The Design and Implementation of a Linescan Camera based Real-time Image Scanning System
  
  * Jeong, Dong Hyun and Kim, Young Rin and Lee, Kang Moon and Jin, Kwang Won and Song, Chang Geun and others
  
  * ```
    @article{jeongdesign,
      title={The Design and Implementation of a Linescan Camera based Real-time Image Scanning System},
      author={Jeong, Dong Hyun and Kim, Young Rin and Lee, Kang Moon and Jin, Kwang Won and Song, Chang Geun and others}
    }
    ```
  
    
  
* Coin recognition using line scan camera

  * A. Gavrijaseva; [O. Martens](https://ieeexplore.ieee.org/author/37267653800); [R. Land](https://ieeexplore.ieee.org/author/38513020700); [M. Reidla](https://ieeexplore.ieee.org/author/37586702500)

  * https://ieeexplore.ieee.org/abstract/document/7320581

  * ```latex
    @INPROCEEDINGS{7320581,
      author={A. {Gavrijaseva} and O. {Martens} and R. {Land} and M. {Reidla}},
      booktitle={2014 14th Biennial Baltic Electronic Conference (BEC)}, 
      title={Coin recognition using line scan camera}, 
      year={2014},
      volume={},
      number={},
      pages={161-164},
      doi={10.1109/BEC.2014.7320581}}
    ```

* Optic sensor for the joint of a walking robot
  * Todor Stoyanov Djamiykov, Mladen Stoilov Milushev, Marin Berov Marinov
  * http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.621.1414&rep=rep1&type=pdf
  * 
    ```latex
    @article{djamiykov2007optic,
      title={OPTIC SENSOR FOR THE JOINT OF A WALKING ROBOT},
      author={Djamiykov, Todor Stoyanov and Milushev, Mladen Stoilov and Marinov, Marin Berov},
      journal={Proceedings of the Technical University--Sofia},
      volume={57},
      number={2},
      pages={201--206},
      year={2007},
      publisher={Citeseer}
    }
    ```
  
* no - Pattern Optimization for 3D Surface Reconstruction with an Active Line Scan Camera System

  * [Erik Lilienblum](https://ieeexplore-1ieee-1org-1llq4z3h202ac.han.ubl.jku.at/author/37703853800); [Ayoub Al-Hamadi](https://ieeexplore-1ieee-1org-1llq4z3h202ac.han.ubl.jku.at/author/37373438500)
  * https://ieeexplore-1ieee-1org-1llq4z3h202ac.han.ubl.jku.at/document/8451807
  * 
    ```latex
    @INPROCEEDINGS{8451807,
  author={E. {Lilienblum} and A. {Al-Hamadi}},
  booktitle={2018 25th IEEE International Conference on Image Processing (ICIP)}, 
  title={Pattern Optimization for 3D Surface Reconstruction with an Active Line Scan Camera System}, 
  year={2018},
  volume={},
  number={},
  pages={3159-3163},
  doi={10.1109/ICIP.2018.8451807}}
    ```
  
* Vision-based detection of events using line-scan camera
  * Mirosław Jabłonski, Ryszard Tadeusiewicz
  * https://ieeexplore-1ieee-1org-1llq4z3h202ac.han.ubl.jku.at/document/7605275
  * 
    ```latex
    @INPROCEEDINGS{7605275,
  author={M. {Jabłoński} and R. {Tadeusiewicz}},
  booktitle={2016 Second International Conference on Event-based Control, Communication, and Signal Processing (EBCCSP)}, 
  title={Vision-based detection of events using line-scan camera}, 
  year={2016},
  volume={},
  number={},
  pages={1-3},
  doi={10.1109/EBCCSP.2016.7605275}}
    ```
  